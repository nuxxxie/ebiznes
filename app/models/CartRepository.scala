package models

import javax.inject.{Inject, Singleton}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile
import models.{Category, CategoryRepository, ExtendedProduct, Product, ProductRepository, Promotion, PromotionRepository, Stock, StockRepository}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class CartRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {
  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class CartTable(tag: Tag) extends Table[Cart](tag, "carts") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def user_id = column[String]("user_id")
    def product_id = column[Int]("product_id")
    def quantity = column[Int]("quantity")

    def * = (id, user_id, product_id, quantity) <> ((Cart.apply _).tupled, Cart.unapply)
  }

  private val cart = TableQuery[CartTable]

  def add(user_id: String, product_id: Int, quantity: Int): Future[Cart] = db.run {
    (cart.map(c => (c.user_id, c.product_id, c.quantity))
      returning cart.map(_.id)
      into { case ((user_id, product_id, quantity), id) => Cart(id, user_id, product_id, quantity) }
      )  += (user_id, product_id, quantity)
  }

  def list(): Future[Seq[Cart]] = db.run {
    cart.result
  }

  def detailsUser(uid: String): Future[Seq[Cart]] = db.run {
    cart.filter(_.user_id === uid).result
  }

  def delete(uid: String): Future[Unit] = db.run {
    cart.filter(_.user_id === uid)
      .delete
      .map(_ => ())
  }

  def update(id: Int, updated: Cart): Future[Cart] = {
    val toUpdate: Cart = updated.copy(id)
    db.run {
      cart.filter(_.id === id)
        .update(toUpdate)
        .map(_ => toUpdate)
    }
  }
}