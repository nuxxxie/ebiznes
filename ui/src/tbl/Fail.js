import React from "react";

export class Fail extends React.Component {
    render() {
        return (
            <div>
                <h3>Woops, something went wrong!</h3>
            </div>
        );
    }
}