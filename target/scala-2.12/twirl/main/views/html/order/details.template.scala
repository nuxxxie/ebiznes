
package views.html.order

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object details extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Order,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(order: Order):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*3.1*/("""<table aria-describedby="orders">
    <tr>
        <td id="id">ID: """),_display_(/*5.26*/order/*5.31*/.id),format.raw/*5.34*/("""</td>
    </tr>
    <tr>
        <td id="user_id">User id: """),_display_(/*8.36*/order/*8.41*/.user_id),format.raw/*8.49*/("""</td>
    </tr>
</table>"""))
      }
    }
  }

  def render(order:Order): play.twirl.api.HtmlFormat.Appendable = apply(order)

  def f:((Order) => play.twirl.api.HtmlFormat.Appendable) = (order) => apply(order)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.452
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/order/details.scala.html
                  HASH: b91d04536b724aff3fa96def0d172d5b4b5ee587
                  MATRIX: 736->1|844->16|871->17|965->85|978->90|1001->93|1087->153|1100->158|1128->166
                  LINES: 21->1|26->2|27->3|29->5|29->5|29->5|32->8|32->8|32->8
                  -- GENERATED --
              */
          