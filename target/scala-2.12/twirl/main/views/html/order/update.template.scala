
package views.html.order

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object update extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[Form[UpdateOrderForm],MessagesRequestHeader,Flash,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(order: Form[UpdateOrderForm])(implicit request: MessagesRequestHeader, flash: Flash):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*3.2*/import helper._


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*4.1*/("""
"""),_display_(/*5.2*/flash/*5.7*/.get("success").getOrElse("")),format.raw/*5.36*/("""

"""),_display_(/*7.2*/form(routes.OrderController.updateHandle)/*7.43*/ {_display_(Seq[Any](format.raw/*7.45*/("""

    """),_display_(/*9.6*/helper/*9.12*/.CSRF.formField),format.raw/*9.27*/("""
    """),format.raw/*10.5*/("""<input name="id" id="id" value=""""),_display_(/*10.38*/order("id")/*10.49*/.value),format.raw/*10.55*/("""" type="hidden" />
    """),_display_(/*11.6*/inputText(order("user_id"))),format.raw/*11.33*/("""

    """),format.raw/*13.5*/("""<div class="buttons">
        <input type="submit" value="Update order"/>
    </div>

""")))}))
      }
    }
  }

  def render(order:Form[UpdateOrderForm],request:MessagesRequestHeader,flash:Flash): play.twirl.api.HtmlFormat.Appendable = apply(order)(request,flash)

  def f:((Form[UpdateOrderForm]) => (MessagesRequestHeader,Flash) => play.twirl.api.HtmlFormat.Appendable) = (order) => (request,flash) => apply(order)(request,flash)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.470
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/order/update.scala.html
                  HASH: e82dd0362fb0de8f2495bf333538d34b77389651
                  MATRIX: 779->1|937->89|981->87|1008->105|1035->107|1047->112|1096->141|1124->144|1173->185|1212->187|1244->194|1258->200|1293->215|1325->220|1385->253|1405->264|1432->270|1482->294|1530->321|1563->327
                  LINES: 21->1|24->3|27->2|28->4|29->5|29->5|29->5|31->7|31->7|31->7|33->9|33->9|33->9|34->10|34->10|34->10|34->10|35->11|35->11|37->13
                  -- GENERATED --
              */
          