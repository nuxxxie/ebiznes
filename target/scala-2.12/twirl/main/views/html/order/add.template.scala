
package views.html.order

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object add extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[Form[CreateOrderForm],MessagesRequestHeader,Flash,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(order: Form[CreateOrderForm])(implicit request: MessagesRequestHeader, flash: Flash):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*3.2*/import helper._


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*4.1*/("""
"""),_display_(/*5.2*/flash/*5.7*/.get("success").getOrElse("")),format.raw/*5.36*/("""

"""),_display_(/*7.2*/form(routes.OrderController.createHandle())/*7.45*/ {_display_(Seq[Any](format.raw/*7.47*/("""
    """),_display_(/*8.6*/helper/*8.12*/.CSRF.formField),format.raw/*8.27*/("""
    """),_display_(/*9.6*/inputText(order("user_id"))),format.raw/*9.33*/("""

    """),format.raw/*11.5*/("""<div class="buttons">
        <input type="submit" value="Create order"/>
    </div>
""")))}))
      }
    }
  }

  def render(order:Form[CreateOrderForm],request:MessagesRequestHeader,flash:Flash): play.twirl.api.HtmlFormat.Appendable = apply(order)(request,flash)

  def f:((Form[CreateOrderForm]) => (MessagesRequestHeader,Flash) => play.twirl.api.HtmlFormat.Appendable) = (order) => (request,flash) => apply(order)(request,flash)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.458
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/order/add.scala.html
                  HASH: 76a4fbb4ed0af8aeef9e2aa052f66db6c1cafe04
                  MATRIX: 776->1|934->89|978->87|1005->105|1032->107|1044->112|1093->141|1121->144|1172->187|1211->189|1242->195|1256->201|1291->216|1322->222|1369->249|1402->255
                  LINES: 21->1|24->3|27->2|28->4|29->5|29->5|29->5|31->7|31->7|31->7|32->8|32->8|32->8|33->9|33->9|35->11
                  -- GENERATED --
              */
          