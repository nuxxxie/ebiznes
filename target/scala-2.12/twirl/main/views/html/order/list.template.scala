
package views.html.order

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object list extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Seq[Order],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(orders: Seq[Order]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*3.1*/("""<table aria-describedby="orders">
    <tr>
        <th id="id">ID</th>
        <th id="user_id">User ID</th>
    </tr>
    """),_display_(/*8.6*/for(order <- orders) yield /*8.26*/ {_display_(Seq[Any](format.raw/*8.28*/("""
        """),format.raw/*9.9*/("""<tr>
            <td>"""),_display_(/*10.18*/order/*10.23*/.id),format.raw/*10.26*/("""</td>
            <td>"""),_display_(/*11.18*/order/*11.23*/.user_id),format.raw/*11.31*/("""</td>
        </tr>
    """)))}),format.raw/*13.6*/("""
"""),format.raw/*14.1*/("""</table>
"""))
      }
    }
  }

  def render(orders:Seq[Order]): play.twirl.api.HtmlFormat.Appendable = apply(orders)

  def f:((Seq[Order]) => play.twirl.api.HtmlFormat.Appendable) = (orders) => apply(orders)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.464
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/order/list.scala.html
                  HASH: 9a7a2d771b1a1b44ec775403d9d6b181cc3804a3
                  MATRIX: 738->1|852->22|879->23|1028->147|1063->167|1102->169|1137->178|1186->200|1200->205|1224->208|1274->231|1288->236|1317->244|1372->269|1400->270
                  LINES: 21->1|26->2|27->3|32->8|32->8|32->8|33->9|34->10|34->10|34->10|35->11|35->11|35->11|37->13|38->14
                  -- GENERATED --
              */
          