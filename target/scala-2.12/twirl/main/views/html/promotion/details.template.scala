
package views.html.promotion

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object details extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Promotion,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(promotion: Promotion):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*3.1*/("""<table aria-describedby="promotions">
    <tr>
        <td id="id">ID: """),_display_(/*5.26*/promotion/*5.35*/.id),format.raw/*5.38*/("""</td>
    </tr>
    <tr>
        <td id="name">Name: """),_display_(/*8.30*/promotion/*8.39*/.name),format.raw/*8.44*/("""</td>
    </tr>
    <tr>
        <td id="flag_active">Is promo active?: """),_display_(/*11.49*/promotion/*11.58*/.flag_active),format.raw/*11.70*/("""</td>
    </tr>
    <tr>
        <td id="product_id">Product ID: """),_display_(/*14.42*/promotion/*14.51*/.product_id),format.raw/*14.62*/("""</td>
    </tr>
    <tr>
        <td id="percentage_sale">Perc. sale: """),_display_(/*17.47*/promotion/*17.56*/.percentage_sale),format.raw/*17.72*/("""</td>
    </tr>
</table>"""))
      }
    }
  }

  def render(promotion:Promotion): play.twirl.api.HtmlFormat.Appendable = apply(promotion)

  def f:((Promotion) => play.twirl.api.HtmlFormat.Appendable) = (promotion) => apply(promotion)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.419
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/promotion/details.scala.html
                  HASH: 35cbfd2b70a22f16edec346db98ad18339a2d0e2
                  MATRIX: 744->1|860->24|887->25|985->97|1002->106|1025->109|1105->163|1122->172|1147->177|1247->250|1265->259|1298->271|1391->337|1409->346|1441->357|1539->428|1557->437|1594->453
                  LINES: 21->1|26->2|27->3|29->5|29->5|29->5|32->8|32->8|32->8|35->11|35->11|35->11|38->14|38->14|38->14|41->17|41->17|41->17
                  -- GENERATED --
              */
          