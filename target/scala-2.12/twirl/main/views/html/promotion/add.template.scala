
package views.html.promotion

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object add extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[Form[CreatePromotionForm],MessagesRequestHeader,Flash,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(promotion: Form[CreatePromotionForm])(implicit request: MessagesRequestHeader, flash: Flash):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*3.2*/import helper._


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*4.1*/("""
"""),_display_(/*5.2*/flash/*5.7*/.get("success").getOrElse("")),format.raw/*5.36*/("""

"""),_display_(/*7.2*/form(routes.PromotionController.createHandle())/*7.49*/ {_display_(Seq[Any](format.raw/*7.51*/("""
    """),_display_(/*8.6*/helper/*8.12*/.CSRF.formField),format.raw/*8.27*/("""
    """),_display_(/*9.6*/inputText(promotion("name"))),format.raw/*9.34*/("""
    """),_display_(/*10.6*/inputText(promotion("flag_active"))),format.raw/*10.41*/("""
    """),_display_(/*11.6*/inputText(promotion("product_id"))),format.raw/*11.40*/("""
    """),_display_(/*12.6*/inputText(promotion("percentage_sale"))),format.raw/*12.45*/("""

    """),format.raw/*14.5*/("""<div class="buttons">
        <input type="submit" value="Create promotion"/>
    </div>
""")))}))
      }
    }
  }

  def render(promotion:Form[CreatePromotionForm],request:MessagesRequestHeader,flash:Flash): play.twirl.api.HtmlFormat.Appendable = apply(promotion)(request,flash)

  def f:((Form[CreatePromotionForm]) => (MessagesRequestHeader,Flash) => play.twirl.api.HtmlFormat.Appendable) = (promotion) => (request,flash) => apply(promotion)(request,flash)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.429
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/promotion/add.scala.html
                  HASH: ce678edb596f8cff60356b8a3a9ab11c9d949ba3
                  MATRIX: 784->1|950->97|994->95|1021->113|1048->115|1060->120|1109->149|1137->152|1192->199|1231->201|1262->207|1276->213|1311->228|1342->234|1390->262|1422->268|1478->303|1510->309|1565->343|1597->349|1657->388|1690->394
                  LINES: 21->1|24->3|27->2|28->4|29->5|29->5|29->5|31->7|31->7|31->7|32->8|32->8|32->8|33->9|33->9|34->10|34->10|35->11|35->11|36->12|36->12|38->14
                  -- GENERATED --
              */
          