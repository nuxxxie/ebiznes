
package views.html.promotion

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object update extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[Form[UpdatePromotionForm],MessagesRequestHeader,Flash,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(promotion: Form[UpdatePromotionForm])(implicit request: MessagesRequestHeader, flash: Flash):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*3.2*/import helper._


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*4.1*/("""
"""),_display_(/*5.2*/flash/*5.7*/.get("success").getOrElse("")),format.raw/*5.36*/("""

"""),_display_(/*7.2*/form(routes.PromotionController.updateHandle)/*7.47*/ {_display_(Seq[Any](format.raw/*7.49*/("""

    """),_display_(/*9.6*/helper/*9.12*/.CSRF.formField),format.raw/*9.27*/("""
    """),format.raw/*10.5*/("""<input name="id" id="id" value=""""),_display_(/*10.38*/promotion("id")/*10.53*/.value),format.raw/*10.59*/("""" type="hidden" />
    """),_display_(/*11.6*/inputText(promotion("name"))),format.raw/*11.34*/("""
    """),_display_(/*12.6*/inputText(promotion("flag_active"))),format.raw/*12.41*/("""
    """),_display_(/*13.6*/inputText(promotion("product_id"))),format.raw/*13.40*/("""
    """),_display_(/*14.6*/inputText(promotion("percentage_sale"))),format.raw/*14.45*/("""

    """),format.raw/*16.5*/("""<div class="buttons">
        <input type="submit" value="Update promotion"/>
    </div>

""")))}))
      }
    }
  }

  def render(promotion:Form[UpdatePromotionForm],request:MessagesRequestHeader,flash:Flash): play.twirl.api.HtmlFormat.Appendable = apply(promotion)(request,flash)

  def f:((Form[UpdatePromotionForm]) => (MessagesRequestHeader,Flash) => play.twirl.api.HtmlFormat.Appendable) = (promotion) => (request,flash) => apply(promotion)(request,flash)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.446
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/promotion/update.scala.html
                  HASH: 66ea8dad3ffaa5ad29c65009be111226e31de2c2
                  MATRIX: 787->1|953->97|997->95|1024->113|1051->115|1063->120|1112->149|1140->152|1193->197|1232->199|1264->206|1278->212|1313->227|1345->232|1405->265|1429->280|1456->286|1506->310|1555->338|1587->344|1643->379|1675->385|1730->419|1762->425|1822->464|1855->470
                  LINES: 21->1|24->3|27->2|28->4|29->5|29->5|29->5|31->7|31->7|31->7|33->9|33->9|33->9|34->10|34->10|34->10|34->10|35->11|35->11|36->12|36->12|37->13|37->13|38->14|38->14|40->16
                  -- GENERATED --
              */
          