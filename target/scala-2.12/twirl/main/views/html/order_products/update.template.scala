
package views.html.order_products

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object update extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[Form[UpdateOrderProductsForm],MessagesRequestHeader,Flash,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(order_products: Form[UpdateOrderProductsForm])(implicit request: MessagesRequestHeader, flash: Flash):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*3.2*/import helper._


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*4.1*/("""
"""),_display_(/*5.2*/flash/*5.7*/.get("success").getOrElse("")),format.raw/*5.36*/("""

"""),_display_(/*7.2*/form(routes.OrderProductsController.updateHandle)/*7.51*/ {_display_(Seq[Any](format.raw/*7.53*/("""

    """),_display_(/*9.6*/helper/*9.12*/.CSRF.formField),format.raw/*9.27*/("""
    """),format.raw/*10.5*/("""<input name="id" id="id" value=""""),_display_(/*10.38*/order_products("id")/*10.58*/.value),format.raw/*10.64*/("""" type="hidden" />
    """),_display_(/*11.6*/inputText(order_products("order_id"))),format.raw/*11.43*/("""
    """),_display_(/*12.6*/inputText(order_products("product_id"))),format.raw/*12.45*/("""
    """),_display_(/*13.6*/inputText(order_products("quantity"))),format.raw/*13.43*/("""

    """),format.raw/*15.5*/("""<div class="buttons">
        <input type="submit" value="Update order products"/>
    </div>

""")))}))
      }
    }
  }

  def render(order_products:Form[UpdateOrderProductsForm],request:MessagesRequestHeader,flash:Flash): play.twirl.api.HtmlFormat.Appendable = apply(order_products)(request,flash)

  def f:((Form[UpdateOrderProductsForm]) => (MessagesRequestHeader,Flash) => play.twirl.api.HtmlFormat.Appendable) = (order_products) => (request,flash) => apply(order_products)(request,flash)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.613
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/order_products/update.scala.html
                  HASH: 57bc41dfc85b68fb58a8a326703ff2c7b16f1e72
                  MATRIX: 796->1|971->106|1015->104|1042->122|1069->124|1081->129|1130->158|1158->161|1215->210|1254->212|1286->219|1300->225|1335->240|1367->245|1427->278|1456->298|1483->304|1533->328|1591->365|1623->371|1683->410|1715->416|1773->453|1806->459
                  LINES: 21->1|24->3|27->2|28->4|29->5|29->5|29->5|31->7|31->7|31->7|33->9|33->9|33->9|34->10|34->10|34->10|34->10|35->11|35->11|36->12|36->12|37->13|37->13|39->15
                  -- GENERATED --
              */
          