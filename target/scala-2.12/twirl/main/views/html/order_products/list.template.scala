
package views.html.order_products

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object list extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Seq[OrderProducts],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(orders_products: Seq[OrderProducts]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*3.1*/("""<table aria-describedby="orders_products">
    <tr>
        <th id="id">ID</th>
        <th id="order_id">Order ID</th>
        <th id="product_id">Product ID</th>
        <th id="quantity">Quantity</th>
    </tr>
    """),_display_(/*10.6*/for(order_products <- orders_products) yield /*10.44*/ {_display_(Seq[Any](format.raw/*10.46*/("""
        """),format.raw/*11.9*/("""<tr>
            <td>"""),_display_(/*12.18*/order_products/*12.32*/.id),format.raw/*12.35*/("""</td>
            <td>"""),_display_(/*13.18*/order_products/*13.32*/.order_id),format.raw/*13.41*/("""</td>
            <td>"""),_display_(/*14.18*/order_products/*14.32*/.product_id),format.raw/*14.43*/("""</td>
            <td>"""),_display_(/*15.18*/order_products/*15.32*/.quantity),format.raw/*15.41*/("""</td>
        </tr>
    """)))}),format.raw/*17.6*/("""
"""),format.raw/*18.1*/("""</table>"""))
      }
    }
  }

  def render(orders_products:Seq[OrderProducts]): play.twirl.api.HtmlFormat.Appendable = apply(orders_products)

  def f:((Seq[OrderProducts]) => play.twirl.api.HtmlFormat.Appendable) = (orders_products) => apply(orders_products)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.605
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/order_products/list.scala.html
                  HASH: a2a3c90d42d6d52465d007de7db61693e5553f98
                  MATRIX: 755->1|886->39|913->40|1158->259|1212->297|1252->299|1288->308|1337->330|1360->344|1384->347|1434->370|1457->384|1487->393|1537->416|1560->430|1592->441|1642->464|1665->478|1695->487|1750->512|1778->513
                  LINES: 21->1|26->2|27->3|34->10|34->10|34->10|35->11|36->12|36->12|36->12|37->13|37->13|37->13|38->14|38->14|38->14|39->15|39->15|39->15|41->17|42->18
                  -- GENERATED --
              */
          