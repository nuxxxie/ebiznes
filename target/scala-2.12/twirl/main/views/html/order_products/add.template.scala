
package views.html.order_products

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object add extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[Form[CreateOrderProductsForm],MessagesRequestHeader,Flash,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(order_products: Form[CreateOrderProductsForm])(implicit request: MessagesRequestHeader, flash: Flash):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*3.2*/import helper._


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*4.1*/("""
"""),_display_(/*5.2*/flash/*5.7*/.get("success").getOrElse("")),format.raw/*5.36*/("""

"""),_display_(/*7.2*/form(routes.OrderProductsController.addHandle())/*7.50*/ {_display_(Seq[Any](format.raw/*7.52*/("""
    """),_display_(/*8.6*/helper/*8.12*/.CSRF.formField),format.raw/*8.27*/("""
    """),_display_(/*9.6*/inputText(order_products("order_id"))),format.raw/*9.43*/("""
    """),_display_(/*10.6*/inputText(order_products("product_id"))),format.raw/*10.45*/("""
    """),_display_(/*11.6*/inputText(order_products("quantity"))),format.raw/*11.43*/("""

    """),format.raw/*13.5*/("""<div class="buttons">
        <input type="submit" value="Add to order"/>
    </div>
""")))}))
      }
    }
  }

  def render(order_products:Form[CreateOrderProductsForm],request:MessagesRequestHeader,flash:Flash): play.twirl.api.HtmlFormat.Appendable = apply(order_products)(request,flash)

  def f:((Form[CreateOrderProductsForm]) => (MessagesRequestHeader,Flash) => play.twirl.api.HtmlFormat.Appendable) = (order_products) => (request,flash) => apply(order_products)(request,flash)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.598
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/order_products/add.scala.html
                  HASH: e3b762f56684d3479110d598301fe41f9ab0dbf0
                  MATRIX: 793->1|968->106|1012->104|1039->122|1066->124|1078->129|1127->158|1155->161|1211->209|1250->211|1281->217|1295->223|1330->238|1361->244|1418->281|1450->287|1510->326|1542->332|1600->369|1633->375
                  LINES: 21->1|24->3|27->2|28->4|29->5|29->5|29->5|31->7|31->7|31->7|32->8|32->8|32->8|33->9|33->9|34->10|34->10|35->11|35->11|37->13
                  -- GENERATED --
              */
          