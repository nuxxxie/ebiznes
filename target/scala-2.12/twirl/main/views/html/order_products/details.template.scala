
package views.html.order_products

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object details extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[OrderProducts,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(order_products: OrderProducts):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*3.1*/("""<table aria-describedby="order_products">
    <tr>
        <td id="id">ID: """),_display_(/*5.26*/order_products/*5.40*/.id),format.raw/*5.43*/("""</td>
    </tr>
    <tr>
        <td id="order_id">ID: """),_display_(/*8.32*/order_products/*8.46*/.order_id),format.raw/*8.55*/("""</td>
    </tr>
    <tr>
        <td id="product_id">Order id: """),_display_(/*11.40*/order_products/*11.54*/.product_id),format.raw/*11.65*/("""</td>
    </tr>
    <tr>
        <td id="quantity">Order id: """),_display_(/*14.38*/order_products/*14.52*/.quantity),format.raw/*14.61*/("""</td>
    </tr>
</table>"""))
      }
    }
  }

  def render(order_products:OrderProducts): play.twirl.api.HtmlFormat.Appendable = apply(order_products)

  def f:((OrderProducts) => play.twirl.api.HtmlFormat.Appendable) = (order_products) => apply(order_products)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.590
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/order_products/details.scala.html
                  HASH: c2ef22289470b9a16305ab1bb5977cbd7a4bc7ca
                  MATRIX: 753->1|878->33|905->34|1007->110|1029->124|1052->127|1134->183|1156->197|1185->206|1276->270|1299->284|1331->295|1420->357|1443->371|1473->380
                  LINES: 21->1|26->2|27->3|29->5|29->5|29->5|32->8|32->8|32->8|35->11|35->11|35->11|38->14|38->14|38->14
                  -- GENERATED --
              */
          