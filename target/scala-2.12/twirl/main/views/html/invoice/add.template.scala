
package views.html.invoice

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object add extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[Form[CreateInvoiceForm],MessagesRequestHeader,Flash,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(invoice: Form[CreateInvoiceForm])(implicit request: MessagesRequestHeader, flash: Flash):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*3.2*/import helper._


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*4.1*/("""
"""),_display_(/*5.2*/flash/*5.7*/.get("success").getOrElse("")),format.raw/*5.36*/("""

"""),_display_(/*7.2*/form(routes.InvoiceController.createHandle())/*7.47*/ {_display_(Seq[Any](format.raw/*7.49*/("""
    """),_display_(/*8.6*/helper/*8.12*/.CSRF.formField),format.raw/*8.27*/("""
    """),_display_(/*9.6*/inputText(invoice("order_id"))),format.raw/*9.36*/("""
    """),_display_(/*10.6*/inputText(invoice("payment_due"))),format.raw/*10.39*/("""

    """),format.raw/*12.5*/("""<div class="buttons">
        <input type="submit" value="Create invoice"/>
    </div>
""")))}),format.raw/*15.2*/("""
"""))
      }
    }
  }

  def render(invoice:Form[CreateInvoiceForm],request:MessagesRequestHeader,flash:Flash): play.twirl.api.HtmlFormat.Appendable = apply(invoice)(request,flash)

  def f:((Form[CreateInvoiceForm]) => (MessagesRequestHeader,Flash) => play.twirl.api.HtmlFormat.Appendable) = (invoice) => (request,flash) => apply(invoice)(request,flash)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.707
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/invoice/add.scala.html
                  HASH: 45279f774616917aa05f28c74101595955deba45
                  MATRIX: 780->1|942->93|986->91|1013->109|1040->111|1052->116|1101->145|1129->148|1182->193|1221->195|1252->201|1266->207|1301->222|1332->228|1382->258|1414->264|1468->297|1501->303|1619->391
                  LINES: 21->1|24->3|27->2|28->4|29->5|29->5|29->5|31->7|31->7|31->7|32->8|32->8|32->8|33->9|33->9|34->10|34->10|36->12|39->15
                  -- GENERATED --
              */
          