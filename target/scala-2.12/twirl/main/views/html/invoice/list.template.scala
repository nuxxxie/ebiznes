
package views.html.invoice

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object list extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Seq[Invoice],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(invoices: Seq[Invoice]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*3.1*/("""<table aria-describedby="invoices">
    <tr>
        <th id="order_id">Order id</th>
        <th id="payment_due">Payment due</th>
    </tr>
    """),_display_(/*8.6*/for(invoice <- invoices) yield /*8.30*/ {_display_(Seq[Any](format.raw/*8.32*/("""
        """),format.raw/*9.9*/("""<tr>
            <td>"""),_display_(/*10.18*/invoice/*10.25*/.order_id),format.raw/*10.34*/("""</td>
            <td>"""),_display_(/*11.18*/invoice/*11.25*/.payment_due),format.raw/*11.37*/("""</td>
        </tr>
    """)))}),format.raw/*13.6*/("""
"""),format.raw/*14.1*/("""</table>
"""))
      }
    }
  }

  def render(invoices:Seq[Invoice]): play.twirl.api.HtmlFormat.Appendable = apply(invoices)

  def f:((Seq[Invoice]) => play.twirl.api.HtmlFormat.Appendable) = (invoices) => apply(invoices)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.714
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/invoice/list.scala.html
                  HASH: 6f7b9222cec1d27362adc5ace91e444bbcbeed4d
                  MATRIX: 742->1|860->26|887->27|1058->173|1097->197|1136->199|1171->208|1220->230|1236->237|1266->246|1316->269|1332->276|1365->288|1420->313|1448->314
                  LINES: 21->1|26->2|27->3|32->8|32->8|32->8|33->9|34->10|34->10|34->10|35->11|35->11|35->11|37->13|38->14
                  -- GENERATED --
              */
          