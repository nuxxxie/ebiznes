
package views.html.invoice

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object update extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[Form[UpdateInvoiceForm],MessagesRequestHeader,Flash,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(invoice: Form[UpdateInvoiceForm])(implicit request: MessagesRequestHeader, flash: Flash):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*3.2*/import helper._


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*4.1*/("""
"""),_display_(/*5.2*/flash/*5.7*/.get("success").getOrElse("")),format.raw/*5.36*/("""

"""),_display_(/*7.2*/form(routes.InvoiceController.updateHandle)/*7.45*/ {_display_(Seq[Any](format.raw/*7.47*/("""

    """),_display_(/*9.6*/helper/*9.12*/.CSRF.formField),format.raw/*9.27*/("""
    """),format.raw/*10.5*/("""<input name="id" id="id" value=""""),_display_(/*10.38*/invoice("id")/*10.51*/.value),format.raw/*10.57*/("""" type="hidden" />
    """),_display_(/*11.6*/inputText(invoice("order_id"))),format.raw/*11.36*/("""
    """),_display_(/*12.6*/inputText(invoice("payment_due"))),format.raw/*12.39*/("""

    """),format.raw/*14.5*/("""<div class="buttons">
        <input type="submit" value="Update invoice"/>
    </div>

""")))}))
      }
    }
  }

  def render(invoice:Form[UpdateInvoiceForm],request:MessagesRequestHeader,flash:Flash): play.twirl.api.HtmlFormat.Appendable = apply(invoice)(request,flash)

  def f:((Form[UpdateInvoiceForm]) => (MessagesRequestHeader,Flash) => play.twirl.api.HtmlFormat.Appendable) = (invoice) => (request,flash) => apply(invoice)(request,flash)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.721
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/invoice/update.scala.html
                  HASH: 053ee132bc18544710d7d67498d54836dfea36d2
                  MATRIX: 783->1|945->93|989->91|1016->109|1043->111|1055->116|1104->145|1132->148|1183->191|1222->193|1254->200|1268->206|1303->221|1335->226|1395->259|1417->272|1444->278|1494->302|1545->332|1577->338|1631->371|1664->377
                  LINES: 21->1|24->3|27->2|28->4|29->5|29->5|29->5|31->7|31->7|31->7|33->9|33->9|33->9|34->10|34->10|34->10|34->10|35->11|35->11|36->12|36->12|38->14
                  -- GENERATED --
              */
          