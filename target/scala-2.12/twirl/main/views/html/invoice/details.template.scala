
package views.html.invoice

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object details extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Invoice,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(invoice: Invoice):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*3.1*/("""<table aria-describedby="invoices">
    <tr>
        <td id="id">ID: """),_display_(/*5.26*/invoice/*5.33*/.id),format.raw/*5.36*/("""</td>
    </tr>
    <tr>
        <td id="order_id">Order id: """),_display_(/*8.38*/invoice/*8.45*/.order_id),format.raw/*8.54*/("""</td>
    </tr>
    <tr>
        <td id="payment_due">Payment due: """),_display_(/*11.44*/invoice/*11.51*/.payment_due),format.raw/*11.63*/("""</td>
    </tr>
</table>"""))
      }
    }
  }

  def render(invoice:Invoice): play.twirl.api.HtmlFormat.Appendable = apply(invoice)

  def f:((Invoice) => play.twirl.api.HtmlFormat.Appendable) = (invoice) => apply(invoice)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.698
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/invoice/details.scala.html
                  HASH: cdbf65ff89cf1d7fb46f074b0ba2f3dc0a63bfb0
                  MATRIX: 740->1|852->20|879->21|975->91|990->98|1013->101|1101->163|1116->170|1145->179|1240->247|1256->254|1289->266
                  LINES: 21->1|26->2|27->3|29->5|29->5|29->5|32->8|32->8|32->8|35->11|35->11|35->11
                  -- GENERATED --
              */
          