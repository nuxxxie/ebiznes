
package views.html.review

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object details extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Review,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(review: Review):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*3.1*/("""<table aria-describedby="reviews">
    <tr>
        <td id="id">Review ID: """),_display_(/*5.33*/review/*5.39*/.id),format.raw/*5.42*/("""</td>
    </tr>
    <tr>
        <td id="product_id">Product id: """),_display_(/*8.42*/review/*8.48*/.product_id),format.raw/*8.59*/("""</td>
    </tr>
    <tr>
        <td id="description">Description: """),_display_(/*11.44*/review/*11.50*/.description),format.raw/*11.62*/("""</td>
    </tr>
</table>"""))
      }
    }
  }

  def render(review:Review): play.twirl.api.HtmlFormat.Appendable = apply(review)

  def f:((Review) => play.twirl.api.HtmlFormat.Appendable) = (review) => apply(review)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.563
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/review/details.scala.html
                  HASH: c6568988e36c1b17d5ecdf8c55f590127b5ff9c5
                  MATRIX: 738->1|848->18|875->19|977->95|991->101|1014->104|1106->170|1120->176|1151->187|1246->255|1261->261|1294->273
                  LINES: 21->1|26->2|27->3|29->5|29->5|29->5|32->8|32->8|32->8|35->11|35->11|35->11
                  -- GENERATED --
              */
          