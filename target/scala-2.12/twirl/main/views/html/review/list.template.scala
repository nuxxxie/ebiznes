
package views.html.review

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object list extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Seq[Review],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(reviews: Seq[Review]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*3.1*/("""<table aria-describedby="reviews">
    <tr>
        <th id="id">Review ID</th>
        <th id="product_id">Product ID</th>
    </tr>
    """),_display_(/*8.6*/for(review <- reviews) yield /*8.28*/ {_display_(Seq[Any](format.raw/*8.30*/("""
        """),format.raw/*9.9*/("""<tr>
            <td>"""),_display_(/*10.18*/review/*10.24*/.id),format.raw/*10.27*/("""</td>
            <td>"""),_display_(/*11.18*/review/*11.24*/.product_id),format.raw/*11.35*/("""</td>
        </tr>
    """)))}),format.raw/*13.6*/("""
"""),format.raw/*14.1*/("""</table>
"""))
      }
    }
  }

  def render(reviews:Seq[Review]): play.twirl.api.HtmlFormat.Appendable = apply(reviews)

  def f:((Seq[Review]) => play.twirl.api.HtmlFormat.Appendable) = (reviews) => apply(reviews)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.575
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/review/list.scala.html
                  HASH: 1ceab4797549cf1f590024f772107f03e6ab34b5
                  MATRIX: 740->1|856->24|883->25|1046->163|1083->185|1122->187|1157->196|1206->218|1221->224|1245->227|1295->250|1310->256|1342->267|1397->292|1425->293
                  LINES: 21->1|26->2|27->3|32->8|32->8|32->8|33->9|34->10|34->10|34->10|35->11|35->11|35->11|37->13|38->14
                  -- GENERATED --
              */
          