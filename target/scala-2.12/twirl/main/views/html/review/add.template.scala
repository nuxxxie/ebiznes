
package views.html.review

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object add extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[Form[CreateReviewForm],MessagesRequestHeader,Flash,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(review: Form[CreateReviewForm])(implicit request: MessagesRequestHeader, flash: Flash):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*3.2*/import helper._


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*4.1*/("""
"""),_display_(/*5.2*/flash/*5.7*/.get("success").getOrElse("")),format.raw/*5.36*/("""

"""),_display_(/*7.2*/form(routes.ReviewController.createHandle())/*7.46*/ {_display_(Seq[Any](format.raw/*7.48*/("""
    """),_display_(/*8.6*/helper/*8.12*/.CSRF.formField),format.raw/*8.27*/("""
    """),_display_(/*9.6*/inputText(review("product_id"))),format.raw/*9.37*/("""
    """),_display_(/*10.6*/inputText(review("description"))),format.raw/*10.38*/("""

    """),format.raw/*12.5*/("""<div class="buttons">
        <input type="submit" value="Create review"/>
    </div>
""")))}),format.raw/*15.2*/("""
"""))
      }
    }
  }

  def render(review:Form[CreateReviewForm],request:MessagesRequestHeader,flash:Flash): play.twirl.api.HtmlFormat.Appendable = apply(review)(request,flash)

  def f:((Form[CreateReviewForm]) => (MessagesRequestHeader,Flash) => play.twirl.api.HtmlFormat.Appendable) = (review) => (request,flash) => apply(review)(request,flash)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.569
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/review/add.scala.html
                  HASH: ab114c89a9eeaf82a1277ed71d11bd1ffeaf34ce
                  MATRIX: 778->1|938->91|982->89|1009->107|1036->109|1048->114|1097->143|1125->146|1177->190|1216->192|1247->198|1261->204|1296->219|1327->225|1378->256|1410->262|1463->294|1496->300|1613->387
                  LINES: 21->1|24->3|27->2|28->4|29->5|29->5|29->5|31->7|31->7|31->7|32->8|32->8|32->8|33->9|33->9|34->10|34->10|36->12|39->15
                  -- GENERATED --
              */
          