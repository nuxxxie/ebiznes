
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object navigation extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Messages,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/()(implicit messages: Messages):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*3.1*/("""<div class="navigation-area" ng-controller="NavigationCtrl">
    <header class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" ng-init="navCollapsed = true" ng-click="navCollapsed = !navCollapsed">
                    <span class="sr-only">"""),_display_(/*8.44*/Messages("toggle.navigation")),format.raw/*8.73*/("""</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Silhouette Play Angular Seed</a>
            </div>
            <div class="collapse navbar-collapse" ng-class=""""),format.raw/*15.61*/("""{"""),format.raw/*15.62*/(""" """),format.raw/*15.63*/("""in: !navCollapsed """),format.raw/*15.81*/("""}"""),format.raw/*15.82*/("""" ng-click="navCollapsed = !navCollapsed">
                <ul class="nav navbar-nav">
                    <!-- User is logged in -->
                    <li ng-if="isAuthenticated()"><a ui-sref="home">"""),_display_(/*18.70*/Messages("home")),format.raw/*18.86*/("""</a></li>

                    <!-- User is logged out -->
                    <li ng-if="!isAuthenticated()"><a ui-sref="signIn">"""),_display_(/*21.73*/Messages("home")),format.raw/*21.89*/("""</a></li>

                    <li><a href="https://github.com/mohiva/play-silhouette-angular-seed">GitHub</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <!-- User is logged in -->
                    <li ng-if="isAuthenticated()"><a ui-sref="home">"""),format.raw/*27.69*/("""{"""),format.raw/*27.70*/("""{"""),format.raw/*27.71*/("""user.fullName ? user.fullName : user.firstName"""),format.raw/*27.117*/("""}"""),format.raw/*27.118*/("""}"""),format.raw/*27.119*/("""</a></li>
                    <li ng-if="isAuthenticated()" ui-sref="signOut"><a href="">"""),_display_(/*28.81*/Messages("sign.out")),format.raw/*28.101*/("""</a></li>

                    <!-- User is logged out -->
                    <li ng-if="!isAuthenticated()"><a ui-sref="signIn">"""),_display_(/*31.73*/Messages("sign.in")),format.raw/*31.92*/("""</a></li>
                    <li ng-if="!isAuthenticated()"><a ui-sref="signUp">"""),_display_(/*32.73*/Messages("sign.up")),format.raw/*32.92*/("""</a></li>
                </ul>
            </div>
        </div>
    </header>
</div>
"""))
      }
    }
  }

  def render(messages:Messages): play.twirl.api.HtmlFormat.Appendable = apply()(messages)

  def f:(() => (Messages) => play.twirl.api.HtmlFormat.Appendable) = () => (messages) => apply()(messages)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-09-05T23:19:08.863
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/navigation.scala.html
                  HASH: e0e59244365f1610a36b3926e37e1322e0e72c1a
                  MATRIX: 736->1|861->33|888->34|1299->419|1348->448|1715->787|1744->788|1773->789|1819->807|1848->808|2078->1011|2115->1027|2273->1158|2310->1174|2648->1484|2677->1485|2706->1486|2781->1532|2811->1533|2841->1534|2958->1624|3000->1644|3158->1775|3198->1794|3307->1876|3347->1895
                  LINES: 21->1|26->2|27->3|32->8|32->8|39->15|39->15|39->15|39->15|39->15|42->18|42->18|45->21|45->21|51->27|51->27|51->27|51->27|51->27|51->27|52->28|52->28|55->31|55->31|56->32|56->32
                  -- GENERATED --
              */
          