
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object index extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.1*/("""<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>hey</title>
    </head>
    <body>
        <a href=""""),_display_(/*8.19*/controllers/*8.30*/.routes.CartController.list()),format.raw/*8.59*/("""">Carts</a>
        <a href=""""),_display_(/*9.19*/controllers/*9.30*/.routes.CategoryController.list()),format.raw/*9.63*/("""">Categories</a>
        <a href=""""),_display_(/*10.19*/controllers/*10.30*/.routes.InvoiceController.list()),format.raw/*10.62*/("""">Invoices</a>
        <a href=""""),_display_(/*11.19*/controllers/*11.30*/.routes.OrderController.list()),format.raw/*11.60*/("""">Orders</a>
        <a href=""""),_display_(/*12.19*/controllers/*12.30*/.routes.OrderProductsController.list()),format.raw/*12.68*/("""">Order Products</a>
        <a href=""""),_display_(/*13.19*/controllers/*13.30*/.routes.ProductController.list()),format.raw/*13.62*/("""">Products</a>
        <a href=""""),_display_(/*14.19*/controllers/*14.30*/.routes.PromotionController.list()),format.raw/*14.64*/("""">Promotions</a>
        <a href=""""),_display_(/*15.19*/controllers/*15.30*/.routes.ReviewController.list()),format.raw/*15.61*/("""">Reviews</a>
        <a href=""""),_display_(/*16.19*/controllers/*16.30*/.routes.StockController.list()),format.raw/*16.60*/("""">Stock</a>
        <a href=""""),_display_(/*17.19*/controllers/*17.30*/.routes.WishlistController.list()),format.raw/*17.63*/("""">Wishlists</a>
        <br> <br>
        <h3> Add </h3>
        <a href=""""),_display_(/*20.19*/controllers/*20.30*/.routes.CartController.add()),format.raw/*20.58*/("""">Carts</a>
        <a href=""""),_display_(/*21.19*/controllers/*21.30*/.routes.CategoryController.create()),format.raw/*21.65*/("""">Categories</a>
        <a href=""""),_display_(/*22.19*/controllers/*22.30*/.routes.InvoiceController.create()),format.raw/*22.64*/("""">Invoices</a>
        <a href=""""),_display_(/*23.19*/controllers/*23.30*/.routes.OrderController.create()),format.raw/*23.62*/("""">Orders</a>
        <a href=""""),_display_(/*24.19*/controllers/*24.30*/.routes.OrderProductsController.add()),format.raw/*24.67*/("""">Order Products</a>
        <a href=""""),_display_(/*25.19*/controllers/*25.30*/.routes.ProductController.create()),format.raw/*25.64*/("""">Products</a>
        <a href=""""),_display_(/*26.19*/controllers/*26.30*/.routes.PromotionController.create()),format.raw/*26.66*/("""">Promotions</a>
        <a href=""""),_display_(/*27.19*/controllers/*27.30*/.routes.ReviewController.create()),format.raw/*27.63*/("""">Reviews</a>
        <a href=""""),_display_(/*28.19*/controllers/*28.30*/.routes.StockController.add()),format.raw/*28.59*/("""">Stock</a>
        <a href=""""),_display_(/*29.19*/controllers/*29.30*/.routes.WishlistController.add()),format.raw/*29.62*/("""">Wishlists</a>
    </body>
</html>
    """))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-09-07T07:23:05.276
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/index.scala.html
                  HASH: f717f381db5d0d564419b9249337f7963ffc0d33
                  MATRIX: 811->0|980->143|999->154|1048->183|1104->213|1123->224|1176->257|1238->292|1258->303|1311->335|1371->368|1391->379|1442->409|1500->440|1520->451|1579->489|1645->528|1665->539|1718->571|1778->604|1798->615|1853->649|1915->684|1935->695|1987->726|2046->758|2066->769|2117->799|2174->829|2194->840|2248->873|2350->948|2370->959|2419->987|2476->1017|2496->1028|2552->1063|2614->1098|2634->1109|2689->1143|2749->1176|2769->1187|2822->1219|2880->1250|2900->1261|2958->1298|3024->1337|3044->1348|3099->1382|3159->1415|3179->1426|3236->1462|3298->1497|3318->1508|3372->1541|3431->1573|3451->1584|3501->1613|3558->1643|3578->1654|3631->1686
                  LINES: 26->1|33->8|33->8|33->8|34->9|34->9|34->9|35->10|35->10|35->10|36->11|36->11|36->11|37->12|37->12|37->12|38->13|38->13|38->13|39->14|39->14|39->14|40->15|40->15|40->15|41->16|41->16|41->16|42->17|42->17|42->17|45->20|45->20|45->20|46->21|46->21|46->21|47->22|47->22|47->22|48->23|48->23|48->23|49->24|49->24|49->24|50->25|50->25|50->25|51->26|51->26|51->26|52->27|52->27|52->27|53->28|53->28|53->28|54->29|54->29|54->29
                  -- GENERATED --
              */
          