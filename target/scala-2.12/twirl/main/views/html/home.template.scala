
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object home extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[models.User,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(user: models.User):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),_display_(/*3.2*/main("Your Home!")/*3.20*/ {_display_(Seq[Any](format.raw/*3.22*/("""
    """),format.raw/*4.5*/("""<div class="jumbotron">
        <h1>Welcome Home """),_display_(/*5.27*/user/*5.31*/.firstName),format.raw/*5.41*/(""" """),_display_(/*5.43*/user/*5.47*/.lastName),format.raw/*5.56*/("""</h1>
    </div>
""")))}),format.raw/*7.2*/("""
"""))
      }
    }
  }

  def render(user:models.User): play.twirl.api.HtmlFormat.Appendable = apply(user)

  def f:((models.User) => play.twirl.api.HtmlFormat.Appendable) = (user) => apply(user)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-09-07T07:17:43.769
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/home.scala.html
                  HASH: 0994166bb492f0802e642b3461ba0f9f169905f9
                  MATRIX: 733->1|846->21|873->23|899->41|938->43|969->48|1045->98|1057->102|1087->112|1115->114|1127->118|1156->127|1203->145
                  LINES: 21->1|26->2|27->3|27->3|27->3|28->4|29->5|29->5|29->5|29->5|29->5|29->5|31->7
                  -- GENERATED --
              */
          