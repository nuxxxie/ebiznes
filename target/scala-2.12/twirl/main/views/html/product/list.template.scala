
package views.html.product

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object list extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Seq[Product],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(products: Seq[Product]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*3.1*/("""<table aria-describedby="products">
    <tr>
        <th id="id">ID</th>
        <th id="name">Name</th>
        <th id="description">Description</th>
        <th id="category_id">Category</th>
        <th id="price">Price</th>
    </tr>
    """),_display_(/*11.6*/for(product <- products) yield /*11.30*/ {_display_(Seq[Any](format.raw/*11.32*/("""
        """),format.raw/*12.9*/("""<tr>
            <td>"""),_display_(/*13.18*/product/*13.25*/.id),format.raw/*13.28*/("""</td>
            <td>"""),_display_(/*14.18*/product/*14.25*/.name),format.raw/*14.30*/("""</td>
            <td>"""),_display_(/*15.18*/product/*15.25*/.description),format.raw/*15.37*/("""</td>
            <td>"""),_display_(/*16.18*/product/*16.25*/.category_id),format.raw/*16.37*/("""</td>
            <td>"""),_display_(/*17.18*/product/*17.25*/.price),format.raw/*17.31*/("""</td>
        </tr>
    """)))}),format.raw/*19.6*/("""
"""),format.raw/*20.1*/("""</table>
"""))
      }
    }
  }

  def render(products:Seq[Product]): play.twirl.api.HtmlFormat.Appendable = apply(products)

  def f:((Seq[Product]) => play.twirl.api.HtmlFormat.Appendable) = (products) => apply(products)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.539
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/product/list.scala.html
                  HASH: 38d71ea1366b9bbed935dec7116bd3a6ece2f696
                  MATRIX: 742->1|860->26|887->27|1156->270|1196->294|1236->296|1272->305|1321->327|1337->334|1361->337|1411->360|1427->367|1453->372|1503->395|1519->402|1552->414|1602->437|1618->444|1651->456|1701->479|1717->486|1744->492|1799->517|1827->518
                  LINES: 21->1|26->2|27->3|35->11|35->11|35->11|36->12|37->13|37->13|37->13|38->14|38->14|38->14|39->15|39->15|39->15|40->16|40->16|40->16|41->17|41->17|41->17|43->19|44->20
                  -- GENERATED --
              */
          