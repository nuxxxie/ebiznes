
package views.html.product

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object details extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Product,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(product: Product):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*3.1*/("""<table aria-describedby="products">
    <tr>
        <td id="id">ID: """),_display_(/*5.26*/product/*5.33*/.id),format.raw/*5.36*/("""</td>
    </tr>
    <tr>
        <td id="name">Name: """),_display_(/*8.30*/product/*8.37*/.name),format.raw/*8.42*/("""</td>
    </tr>
    <tr>
        <td id="description">Description: """),_display_(/*11.44*/product/*11.51*/.description),format.raw/*11.63*/("""</td>
    </tr>
    <tr>
        <td id="category_id">Category ID: """),_display_(/*14.44*/product/*14.51*/.category_id),format.raw/*14.63*/("""</td>
    </tr>
    <tr>
        <td id="price">Price: """),_display_(/*17.32*/product/*17.39*/.price),format.raw/*17.45*/("""</td>
    </tr>
</table>"""))
      }
    }
  }

  def render(product:Product): play.twirl.api.HtmlFormat.Appendable = apply(product)

  def f:((Product) => play.twirl.api.HtmlFormat.Appendable) = (product) => apply(product)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.525
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/product/details.scala.html
                  HASH: f74b834978adad0a81cddb4e35cc5aa4ede53145
                  MATRIX: 740->1|852->20|879->21|975->91|990->98|1013->101|1093->155|1108->162|1133->167|1228->235|1244->242|1277->254|1372->322|1388->329|1421->341|1504->397|1520->404|1547->410
                  LINES: 21->1|26->2|27->3|29->5|29->5|29->5|32->8|32->8|32->8|35->11|35->11|35->11|38->14|38->14|38->14|41->17|41->17|41->17
                  -- GENERATED --
              */
          