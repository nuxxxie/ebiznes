
package views.html.product

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object update extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[Form[UpdateProductForm],MessagesRequestHeader,Flash,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(product: Form[UpdateProductForm])(implicit request: MessagesRequestHeader, flash: Flash):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*3.2*/import helper._


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*4.1*/("""
"""),_display_(/*5.2*/flash/*5.7*/.get("success").getOrElse("")),format.raw/*5.36*/("""

"""),_display_(/*7.2*/form(routes.ProductController.updateHandle)/*7.45*/ {_display_(Seq[Any](format.raw/*7.47*/("""

    """),_display_(/*9.6*/helper/*9.12*/.CSRF.formField),format.raw/*9.27*/("""
    """),format.raw/*10.5*/("""<input name="id" id="id" value=""""),_display_(/*10.38*/product("id")/*10.51*/.value),format.raw/*10.57*/("""" type="hidden" />
    """),_display_(/*11.6*/inputText(product("name"))),format.raw/*11.32*/("""
    """),_display_(/*12.6*/inputText(product("description"))),format.raw/*12.39*/("""
    """),_display_(/*13.6*/inputText(product("category_id"))),format.raw/*13.39*/("""
    """),_display_(/*14.6*/inputText(product("price"))),format.raw/*14.33*/("""

    """),format.raw/*16.5*/("""<div class="buttons">
        <input type="submit" value="Update product"/>
    </div>

""")))}))
      }
    }
  }

  def render(product:Form[UpdateProductForm],request:MessagesRequestHeader,flash:Flash): play.twirl.api.HtmlFormat.Appendable = apply(product)(request,flash)

  def f:((Form[UpdateProductForm]) => (MessagesRequestHeader,Flash) => play.twirl.api.HtmlFormat.Appendable) = (product) => (request,flash) => apply(product)(request,flash)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.557
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/product/update.scala.html
                  HASH: f8e2e50b1accaf7d1704e8a7a80d1ee7b73360d6
                  MATRIX: 783->1|945->93|989->91|1016->109|1043->111|1055->116|1104->145|1132->148|1183->191|1222->193|1254->200|1268->206|1303->221|1335->226|1395->259|1417->272|1444->278|1494->302|1541->328|1573->334|1627->367|1659->373|1713->406|1745->412|1793->439|1826->445
                  LINES: 21->1|24->3|27->2|28->4|29->5|29->5|29->5|31->7|31->7|31->7|33->9|33->9|33->9|34->10|34->10|34->10|34->10|35->11|35->11|36->12|36->12|37->13|37->13|38->14|38->14|40->16
                  -- GENERATED --
              */
          