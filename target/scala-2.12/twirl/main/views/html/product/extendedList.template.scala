
package views.html.product

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object extendedList extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Seq[ExtendedProduct],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(products: Seq[ExtendedProduct]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*3.1*/("""<table aria-describedby="products">
    <tr>
        <th id="id">ID</th>
        <th id="name">Name</th>
        <th id="description">Description</th>
        <th id="category_id">Category</th>
        <th id="price">Price</th>
        <th id="category_name">Category name</th>
        <th id="stock_quantity">Units in stock</th>
        <th id="percentage_sales">Promotion</th>
    </tr>
    """),_display_(/*14.6*/for(product <- products) yield /*14.30*/ {_display_(Seq[Any](format.raw/*14.32*/("""
        """),format.raw/*15.9*/("""<tr>
            <td>"""),_display_(/*16.18*/product/*16.25*/.id),format.raw/*16.28*/("""</td>
            <td>"""),_display_(/*17.18*/product/*17.25*/.name),format.raw/*17.30*/("""</td>
            <td>"""),_display_(/*18.18*/product/*18.25*/.description),format.raw/*18.37*/("""</td>
            <td>"""),_display_(/*19.18*/product/*19.25*/.category_id),format.raw/*19.37*/("""</td>
            <td>"""),_display_(/*20.18*/product/*20.25*/.price),format.raw/*20.31*/("""</td>
            <td>"""),_display_(/*21.18*/product/*21.25*/.category),format.raw/*21.34*/("""</td>
            <td>"""),_display_(/*22.18*/product/*22.25*/.stock),format.raw/*22.31*/("""</td>
            <td>"""),_display_(/*23.18*/product/*23.25*/.promotion),format.raw/*23.35*/("""</td>
        </tr>
    """)))}),format.raw/*25.6*/("""
"""),format.raw/*26.1*/("""</table>"""))
      }
    }
  }

  def render(products:Seq[ExtendedProduct]): play.twirl.api.HtmlFormat.Appendable = apply(products)

  def f:((Seq[ExtendedProduct]) => play.twirl.api.HtmlFormat.Appendable) = (products) => apply(products)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.549
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/product/extendedList.scala.html
                  HASH: d0dc4517c61624629a2dbb9cd7f799e9fa0f2a1b
                  MATRIX: 758->1|884->34|911->35|1331->429|1371->453|1411->455|1447->464|1496->486|1512->493|1536->496|1586->519|1602->526|1628->531|1678->554|1694->561|1727->573|1777->596|1793->603|1826->615|1876->638|1892->645|1919->651|1969->674|1985->681|2015->690|2065->713|2081->720|2108->726|2158->749|2174->756|2205->766|2260->791|2288->792
                  LINES: 21->1|26->2|27->3|38->14|38->14|38->14|39->15|40->16|40->16|40->16|41->17|41->17|41->17|42->18|42->18|42->18|43->19|43->19|43->19|44->20|44->20|44->20|45->21|45->21|45->21|46->22|46->22|46->22|47->23|47->23|47->23|49->25|50->26
                  -- GENERATED --
              */
          