
package views.html.fields

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object constructor extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[views.html.helper.FieldElements,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(field: views.html.helper.FieldElements):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),_display_(/*3.2*/field/*3.7*/.input),format.raw/*3.13*/("""
"""))
      }
    }
  }

  def render(field:views.html.helper.FieldElements): play.twirl.api.HtmlFormat.Appendable = apply(field)

  def f:((views.html.helper.FieldElements) => play.twirl.api.HtmlFormat.Appendable) = (field) => apply(field)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-09-05T23:19:08.975
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/fields/constructor.scala.html
                  HASH: 09c46084d5a28417dc3b101c23ac5b126ac98195
                  MATRIX: 767->1|901->42|928->44|940->49|966->55
                  LINES: 21->1|26->2|27->3|27->3|27->3
                  -- GENERATED --
              */
          