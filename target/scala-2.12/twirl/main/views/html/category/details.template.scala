
package views.html.category

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object details extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Category,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(category: Category):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*3.1*/("""<table aria-describedby="categories">
    <tr>
        <td id="id">ID: """),_display_(/*5.26*/category/*5.34*/.id),format.raw/*5.37*/("""</td>
    </tr>
    <tr>
        <td id="name">Name: """),_display_(/*8.30*/category/*8.38*/.name),format.raw/*8.43*/("""</td>
    </tr>
</table>"""))
      }
    }
  }

  def render(category:Category): play.twirl.api.HtmlFormat.Appendable = apply(category)

  def f:((Category) => play.twirl.api.HtmlFormat.Appendable) = (category) => apply(category)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.477
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/category/details.scala.html
                  HASH: 74e0510b37e4c95ec357c166de09d182d4effd65
                  MATRIX: 742->1|856->22|883->23|981->95|997->103|1020->106|1100->160|1116->168|1141->173
                  LINES: 21->1|26->2|27->3|29->5|29->5|29->5|32->8|32->8|32->8
                  -- GENERATED --
              */
          