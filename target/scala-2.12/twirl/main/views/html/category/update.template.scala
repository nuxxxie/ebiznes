
package views.html.category

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object update extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[Form[UpdateCategoryForm],MessagesRequestHeader,Flash,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(category: Form[UpdateCategoryForm])(implicit request: MessagesRequestHeader, flash: Flash):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*3.2*/import helper._


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*4.1*/("""
"""),_display_(/*5.2*/flash/*5.7*/.get("success").getOrElse("")),format.raw/*5.36*/("""

"""),_display_(/*7.2*/form(routes.CategoryController.updateHandle)/*7.46*/ {_display_(Seq[Any](format.raw/*7.48*/("""
    """),_display_(/*8.6*/helper/*8.12*/.CSRF.formField),format.raw/*8.27*/("""
    """),format.raw/*9.5*/("""<input name="id" id="id" value=""""),_display_(/*9.38*/category("id")/*9.52*/.value),format.raw/*9.58*/("""" type="hidden" />
    """),_display_(/*10.6*/inputText(category("name"))),format.raw/*10.33*/("""

    """),format.raw/*12.5*/("""<div class="buttons">
        <input type="submit" value="Update category"/>
    </div>
""")))}))
      }
    }
  }

  def render(category:Form[UpdateCategoryForm],request:MessagesRequestHeader,flash:Flash): play.twirl.api.HtmlFormat.Appendable = apply(category)(request,flash)

  def f:((Form[UpdateCategoryForm]) => (MessagesRequestHeader,Flash) => play.twirl.api.HtmlFormat.Appendable) = (category) => (request,flash) => apply(category)(request,flash)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.517
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/category/update.scala.html
                  HASH: be056ff5fa7bb184660ab832c47360304d440da3
                  MATRIX: 785->1|949->95|993->93|1020->111|1047->113|1059->118|1108->147|1136->150|1188->194|1227->196|1258->202|1272->208|1307->223|1338->228|1397->261|1419->275|1445->281|1495->305|1543->332|1576->338
                  LINES: 21->1|24->3|27->2|28->4|29->5|29->5|29->5|31->7|31->7|31->7|32->8|32->8|32->8|33->9|33->9|33->9|33->9|34->10|34->10|36->12
                  -- GENERATED --
              */
          