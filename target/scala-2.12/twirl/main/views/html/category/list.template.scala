
package views.html.category

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object list extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Seq[Category],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(categories: Seq[Category]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*3.1*/("""<table aria-describedby="categories">
    <tr>
        <th id="id">ID</th>
        <th id="name">Name</th>
    </tr>
    """),_display_(/*8.6*/for(category <- categories) yield /*8.33*/ {_display_(Seq[Any](format.raw/*8.35*/("""
        """),format.raw/*9.9*/("""<tr>
            <td>"""),_display_(/*10.18*/category/*10.26*/.id),format.raw/*10.29*/("""</td>
            <td>"""),_display_(/*11.18*/category/*11.26*/.name),format.raw/*11.31*/("""</td>
        </tr>
    """)))}),format.raw/*13.6*/("""
"""),format.raw/*14.1*/("""</table>
"""))
      }
    }
  }

  def render(categories:Seq[Category]): play.twirl.api.HtmlFormat.Appendable = apply(categories)

  def f:((Seq[Category]) => play.twirl.api.HtmlFormat.Appendable) = (categories) => apply(categories)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.510
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/category/list.scala.html
                  HASH: d3e26ee9371a733844fe01ceb62ba98437dab003
                  MATRIX: 744->1|865->29|892->30|1039->152|1081->179|1120->181|1155->190|1204->212|1221->220|1245->223|1295->246|1312->254|1338->259|1393->284|1421->285
                  LINES: 21->1|26->2|27->3|32->8|32->8|32->8|33->9|34->10|34->10|34->10|35->11|35->11|35->11|37->13|38->14
                  -- GENERATED --
              */
          