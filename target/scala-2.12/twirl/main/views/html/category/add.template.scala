
package views.html.category

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object add extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[Form[CreateCategoryForm],MessagesRequestHeader,Flash,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(category: Form[CreateCategoryForm])(implicit request: MessagesRequestHeader, flash: Flash):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*3.2*/import helper._


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*4.1*/("""
"""),_display_(/*5.2*/flash/*5.7*/.get("success").getOrElse("")),format.raw/*5.36*/("""

"""),_display_(/*7.2*/form(routes.CategoryController.createHandle())/*7.48*/ {_display_(Seq[Any](format.raw/*7.50*/("""
    """),_display_(/*8.6*/helper/*8.12*/.CSRF.formField),format.raw/*8.27*/("""
    """),_display_(/*9.6*/inputText(category("name"))),format.raw/*9.33*/("""

    """),format.raw/*11.5*/("""<div class="buttons">
        <input type="submit" value="Create category"/>
    </div>
""")))}),format.raw/*14.2*/("""
"""))
      }
    }
  }

  def render(category:Form[CreateCategoryForm],request:MessagesRequestHeader,flash:Flash): play.twirl.api.HtmlFormat.Appendable = apply(category)(request,flash)

  def f:((Form[CreateCategoryForm]) => (MessagesRequestHeader,Flash) => play.twirl.api.HtmlFormat.Appendable) = (category) => (request,flash) => apply(category)(request,flash)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.504
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/category/add.scala.html
                  HASH: 3bc43cbd32981c829fa5d42df0a3dc28241a2fae
                  MATRIX: 782->1|946->95|990->93|1017->111|1044->113|1056->118|1105->147|1133->150|1187->196|1226->198|1257->204|1271->210|1306->225|1337->231|1384->258|1417->264|1536->353
                  LINES: 21->1|24->3|27->2|28->4|29->5|29->5|29->5|31->7|31->7|31->7|32->8|32->8|32->8|33->9|33->9|35->11|38->14
                  -- GENERATED --
              */
          