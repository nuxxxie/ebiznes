
package views.html.wishlist

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object details extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Wishlist,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(wishlist: Wishlist):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*3.1*/("""<table aria-describedby="wishlists">
    <tr>
        <td id="id">Wishlist ID: """),_display_(/*5.35*/wishlist/*5.43*/.id),format.raw/*5.46*/("""</td>
    </tr>
    <tr>
        <td id="user_id">User ID: """),_display_(/*8.36*/wishlist/*8.44*/.user_id),format.raw/*8.52*/("""</td>
    </tr>
    <tr>
        <td id="product_id">Product ID: """),_display_(/*11.42*/wishlist/*11.50*/.product_id),format.raw/*11.61*/("""</td>
    </tr>
</table>"""))
      }
    }
  }

  def render(wishlist:Wishlist): play.twirl.api.HtmlFormat.Appendable = apply(wishlist)

  def f:((Wishlist) => play.twirl.api.HtmlFormat.Appendable) = (wishlist) => apply(wishlist)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.670
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/wishlist/details.scala.html
                  HASH: 9df46b210f6a9aa60f3bc4aafbe186036ca45006
                  MATRIX: 742->1|856->22|883->23|989->103|1005->111|1028->114|1114->174|1130->182|1158->190|1251->256|1268->264|1300->275
                  LINES: 21->1|26->2|27->3|29->5|29->5|29->5|32->8|32->8|32->8|35->11|35->11|35->11
                  -- GENERATED --
              */
          