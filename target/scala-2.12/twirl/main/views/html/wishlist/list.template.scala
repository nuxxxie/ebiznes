
package views.html.wishlist

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object list extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Seq[Wishlist],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(wishlists: Seq[Wishlist]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*3.1*/("""<table aria-describedby="wishlists">
    <tr>
        <th id="id">Wishlist ID</th>
        <th id="user_id">User ID</th>
    </tr>
    """),_display_(/*8.6*/for(wishlist <- wishlists) yield /*8.32*/ {_display_(Seq[Any](format.raw/*8.34*/("""
        """),format.raw/*9.9*/("""<tr>
            <td>"""),_display_(/*10.18*/wishlist/*10.26*/.id),format.raw/*10.29*/("""</td>
            <td>"""),_display_(/*11.18*/wishlist/*11.26*/.user_id),format.raw/*11.34*/("""</td>
        </tr>
    """)))}),format.raw/*13.6*/("""
"""),format.raw/*14.1*/("""</table>
"""))
      }
    }
  }

  def render(wishlists:Seq[Wishlist]): play.twirl.api.HtmlFormat.Appendable = apply(wishlists)

  def f:((Seq[Wishlist]) => play.twirl.api.HtmlFormat.Appendable) = (wishlists) => apply(wishlists)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.682
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/wishlist/list.scala.html
                  HASH: 3949a636b8092671a7eef35ddb856397fe7a43a6
                  MATRIX: 744->1|864->28|891->29|1052->165|1093->191|1132->193|1167->202|1216->224|1233->232|1257->235|1307->258|1324->266|1353->274|1408->299|1436->300
                  LINES: 21->1|26->2|27->3|32->8|32->8|32->8|33->9|34->10|34->10|34->10|35->11|35->11|35->11|37->13|38->14
                  -- GENERATED --
              */
          