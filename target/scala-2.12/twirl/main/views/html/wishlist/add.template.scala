
package views.html.wishlist

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object add extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[Form[CreateWishlistForm],MessagesRequestHeader,Flash,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(wishlist: Form[CreateWishlistForm])(implicit request: MessagesRequestHeader, flash: Flash):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*3.2*/import helper._


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*4.1*/("""
"""),_display_(/*5.2*/flash/*5.7*/.get("success").getOrElse("")),format.raw/*5.36*/("""

"""),_display_(/*7.2*/form(routes.WishlistController.addHandle())/*7.45*/ {_display_(Seq[Any](format.raw/*7.47*/("""
    """),_display_(/*8.6*/helper/*8.12*/.CSRF.formField),format.raw/*8.27*/("""
    """),_display_(/*9.6*/inputText(wishlist("user_id"))),format.raw/*9.36*/("""
    """),_display_(/*10.6*/inputText(wishlist("product_id"))),format.raw/*10.39*/("""

    """),format.raw/*12.5*/("""<div class="buttons">
        <input type="submit" value="Add wishlist"/>
    </div>
""")))}),format.raw/*15.2*/("""
"""))
      }
    }
  }

  def render(wishlist:Form[CreateWishlistForm],request:MessagesRequestHeader,flash:Flash): play.twirl.api.HtmlFormat.Appendable = apply(wishlist)(request,flash)

  def f:((Form[CreateWishlistForm]) => (MessagesRequestHeader,Flash) => play.twirl.api.HtmlFormat.Appendable) = (wishlist) => (request,flash) => apply(wishlist)(request,flash)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.676
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/wishlist/add.scala.html
                  HASH: acd0447c1ce0105deae43c35edfa5e3ace4a24e8
                  MATRIX: 782->1|946->95|990->93|1017->111|1044->113|1056->118|1105->147|1133->150|1184->193|1223->195|1254->201|1268->207|1303->222|1334->228|1384->258|1416->264|1470->297|1503->303|1619->389
                  LINES: 21->1|24->3|27->2|28->4|29->5|29->5|29->5|31->7|31->7|31->7|32->8|32->8|32->8|33->9|33->9|34->10|34->10|36->12|39->15
                  -- GENERATED --
              */
          