
package views.html.stock

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object list extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Seq[Stock],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(stocks: Seq[Stock]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*3.1*/("""<table aria-describedby="stocks">
    <tr>
        <th id="id">Stock ID</th>
        <th id="product_id">Product ID</th>
    </tr>
    """),_display_(/*8.6*/for(stock <- stocks) yield /*8.26*/ {_display_(Seq[Any](format.raw/*8.28*/("""
        """),format.raw/*9.9*/("""<tr>
            <td>"""),_display_(/*10.18*/stock/*10.23*/.id),format.raw/*10.26*/("""</td>
            <td>"""),_display_(/*11.18*/stock/*11.23*/.product_id),format.raw/*11.34*/("""</td>
        </tr>
    """)))}),format.raw/*13.6*/("""
"""),format.raw/*14.1*/("""</table>
"""))
      }
    }
  }

  def render(stocks:Seq[Stock]): play.twirl.api.HtmlFormat.Appendable = apply(stocks)

  def f:((Seq[Stock]) => play.twirl.api.HtmlFormat.Appendable) = (stocks) => apply(stocks)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.657
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/stock/list.scala.html
                  HASH: 5e599011ca66bca72739ed974c10dfbe5f1c3590
                  MATRIX: 738->1|852->22|879->23|1040->159|1075->179|1114->181|1149->190|1198->212|1212->217|1236->220|1286->243|1300->248|1332->259|1387->284|1415->285
                  LINES: 21->1|26->2|27->3|32->8|32->8|32->8|33->9|34->10|34->10|34->10|35->11|35->11|35->11|37->13|38->14
                  -- GENERATED --
              */
          