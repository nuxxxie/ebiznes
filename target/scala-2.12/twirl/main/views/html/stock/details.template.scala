
package views.html.stock

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object details extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Stock,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(stock: Stock):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*3.1*/("""<table aria-describedby="stocks">
    <tr>
        <td id="id">Stock ID: """),_display_(/*5.32*/stock/*5.37*/.id),format.raw/*5.40*/("""</td>
    </tr>
    <tr>
        <td id="product_id">Product id: """),_display_(/*8.42*/stock/*8.47*/.product_id),format.raw/*8.58*/("""</td>
    </tr>
    <tr>
        <td id="quantity">Quantity: """),_display_(/*11.38*/stock/*11.43*/.quantity),format.raw/*11.52*/("""</td>
    </tr>
</table>"""))
      }
    }
  }

  def render(stock:Stock): play.twirl.api.HtmlFormat.Appendable = apply(stock)

  def f:((Stock) => play.twirl.api.HtmlFormat.Appendable) = (stock) => apply(stock)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.644
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/stock/details.scala.html
                  HASH: 284a3f5bc8610422704994daad1b8cf84a2fc114
                  MATRIX: 736->1|844->16|871->17|971->91|984->96|1007->99|1099->165|1112->170|1143->181|1232->243|1246->248|1276->257
                  LINES: 21->1|26->2|27->3|29->5|29->5|29->5|32->8|32->8|32->8|35->11|35->11|35->11
                  -- GENERATED --
              */
          