
package views.html.stock

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object update extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[Form[UpdateStockForm],MessagesRequestHeader,Flash,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(stock: Form[UpdateStockForm])(implicit request: MessagesRequestHeader, flash: Flash):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*3.2*/import helper._


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*4.1*/("""
"""),_display_(/*5.2*/flash/*5.7*/.get("success").getOrElse("")),format.raw/*5.36*/("""

"""),_display_(/*7.2*/form(routes.StockController.updateHandle)/*7.43*/ {_display_(Seq[Any](format.raw/*7.45*/("""

    """),_display_(/*9.6*/helper/*9.12*/.CSRF.formField),format.raw/*9.27*/("""
    """),format.raw/*10.5*/("""<input name="id" id="id" value=""""),_display_(/*10.38*/stock("id")/*10.49*/.value),format.raw/*10.55*/("""" type="hidden" />
    """),_display_(/*11.6*/inputText(stock("product_id"))),format.raw/*11.36*/("""
    """),_display_(/*12.6*/inputText(stock("quantity"))),format.raw/*12.34*/("""

    """),format.raw/*14.5*/("""<div class="buttons">
        <input type="submit" value="Update stock"/>
    </div>

""")))}))
      }
    }
  }

  def render(stock:Form[UpdateStockForm],request:MessagesRequestHeader,flash:Flash): play.twirl.api.HtmlFormat.Appendable = apply(stock)(request,flash)

  def f:((Form[UpdateStockForm]) => (MessagesRequestHeader,Flash) => play.twirl.api.HtmlFormat.Appendable) = (stock) => (request,flash) => apply(stock)(request,flash)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.665
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/stock/update.scala.html
                  HASH: ef9e7e1a44073af7ceb232baec2d577992c56156
                  MATRIX: 779->1|937->89|981->87|1008->105|1035->107|1047->112|1096->141|1124->144|1173->185|1212->187|1244->194|1258->200|1293->215|1325->220|1385->253|1405->264|1432->270|1482->294|1533->324|1565->330|1614->358|1647->364
                  LINES: 21->1|24->3|27->2|28->4|29->5|29->5|29->5|31->7|31->7|31->7|33->9|33->9|33->9|34->10|34->10|34->10|34->10|35->11|35->11|36->12|36->12|38->14
                  -- GENERATED --
              */
          