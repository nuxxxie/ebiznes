
package views.html.stock

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object add extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[Form[CreateStockForm],MessagesRequestHeader,Flash,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(stock: Form[CreateStockForm])(implicit request: MessagesRequestHeader, flash: Flash):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*3.2*/import helper._


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*4.1*/("""
"""),_display_(/*5.2*/flash/*5.7*/.get("success").getOrElse("")),format.raw/*5.36*/("""

"""),_display_(/*7.2*/form(routes.StockController.addHandle())/*7.42*/ {_display_(Seq[Any](format.raw/*7.44*/("""
    """),_display_(/*8.6*/helper/*8.12*/.CSRF.formField),format.raw/*8.27*/("""
    """),_display_(/*9.6*/inputText(stock("product_id"))),format.raw/*9.36*/("""
    """),_display_(/*10.6*/inputText(stock("quantity"))),format.raw/*10.34*/("""

    """),format.raw/*12.5*/("""<div class="buttons">
        <input type="submit" value="Add stock"/>
    </div>
""")))}),format.raw/*15.2*/("""
"""))
      }
    }
  }

  def render(stock:Form[CreateStockForm],request:MessagesRequestHeader,flash:Flash): play.twirl.api.HtmlFormat.Appendable = apply(stock)(request,flash)

  def f:((Form[CreateStockForm]) => (MessagesRequestHeader,Flash) => play.twirl.api.HtmlFormat.Appendable) = (stock) => (request,flash) => apply(stock)(request,flash)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-08-29T23:13:36.651
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/stock/add.scala.html
                  HASH: ead1dc0fe8614929e660da5d646c73decea5c5a1
                  MATRIX: 776->1|934->89|978->87|1005->105|1032->107|1044->112|1093->141|1121->144|1169->184|1208->186|1239->192|1253->198|1288->213|1319->219|1369->249|1401->255|1450->283|1483->289|1596->372
                  LINES: 21->1|24->3|27->2|28->4|29->5|29->5|29->5|31->7|31->7|31->7|32->8|32->8|32->8|33->9|33->9|34->10|34->10|36->12|39->15
                  -- GENERATED --
              */
          