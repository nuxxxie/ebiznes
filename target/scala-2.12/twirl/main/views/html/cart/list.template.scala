
package views.html.cart

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object list extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Seq[Cart],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(carts: Seq[Cart]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*3.1*/("""<table aria-describedby="carts">
    <tr>
        <th id="user_id">User ID</th>
        <th id="product_id">Product ID</th>
        <th id="quantity">Quantity</th>
    </tr>
    """),_display_(/*9.6*/for(cart <- carts) yield /*9.24*/ {_display_(Seq[Any](format.raw/*9.26*/("""
        """),format.raw/*10.9*/("""<tr>
            <td>"""),_display_(/*11.18*/cart/*11.22*/.user_id),format.raw/*11.30*/("""</td>
            <td>"""),_display_(/*12.18*/cart/*12.22*/.product_id),format.raw/*12.33*/("""</td>
            <td>"""),_display_(/*13.18*/cart/*13.22*/.quantity),format.raw/*13.31*/("""</td>
        </tr>
    """)))}),format.raw/*15.6*/("""
"""),format.raw/*16.1*/("""</table>
"""))
      }
    }
  }

  def render(carts:Seq[Cart]): play.twirl.api.HtmlFormat.Appendable = apply(carts)

  def f:((Seq[Cart]) => play.twirl.api.HtmlFormat.Appendable) = (carts) => apply(carts)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-09-12T01:11:22.352
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/cart/list.scala.html
                  HASH: 36edf7365f27f298607e6b198c56ec885a357275
                  MATRIX: 736->1|848->20|875->21|1079->200|1112->218|1151->220|1187->229|1236->251|1249->255|1278->263|1328->286|1341->290|1373->301|1423->324|1436->328|1466->337|1521->362|1549->363
                  LINES: 21->1|26->2|27->3|33->9|33->9|33->9|34->10|35->11|35->11|35->11|36->12|36->12|36->12|37->13|37->13|37->13|39->15|40->16
                  -- GENERATED --
              */
          