
package views.html.cart

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object details extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Seq[Cart],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(carts: Seq[Cart]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*3.1*/("""<table aria-describedby="carts">
    <tr>
        <th id="product_id">Product ID</th>
        <th id="quantity">Quantity</th>
    </tr>
    """),_display_(/*8.6*/for(cart <- carts) yield /*8.24*/ {_display_(Seq[Any](format.raw/*8.26*/("""
        """),format.raw/*9.9*/("""<tr>
            <td>"""),_display_(/*10.18*/cart/*10.22*/.product_id),format.raw/*10.33*/("""</td>
            <td>"""),_display_(/*11.18*/cart/*11.22*/.quantity),format.raw/*11.31*/("""</td>
        </tr>
    """)))}),format.raw/*13.6*/("""
"""),format.raw/*14.1*/("""</table>
"""))
      }
    }
  }

  def render(carts:Seq[Cart]): play.twirl.api.HtmlFormat.Appendable = apply(carts)

  def f:((Seq[Cart]) => play.twirl.api.HtmlFormat.Appendable) = (carts) => apply(carts)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-09-12T01:10:28.196
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/cart/details.scala.html
                  HASH: 965d0b94a621194ba39d6fa72525669f4707cda8
                  MATRIX: 739->1|851->20|878->21|1044->162|1077->180|1116->182|1151->191|1200->213|1213->217|1245->228|1295->251|1308->255|1338->264|1393->289|1421->290
                  LINES: 21->1|26->2|27->3|32->8|32->8|32->8|33->9|34->10|34->10|34->10|35->11|35->11|35->11|37->13|38->14
                  -- GENERATED --
              */
          