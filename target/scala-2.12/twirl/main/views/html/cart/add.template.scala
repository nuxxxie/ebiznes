
package views.html.cart

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object add extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template3[Form[CreateCartForm],MessagesRequestHeader,Flash,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(cart: Form[CreateCartForm])(implicit request: MessagesRequestHeader, flash: Flash):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
/*3.2*/import helper._


Seq[Any](format.raw/*2.1*/("""
"""),format.raw/*4.1*/("""
"""),_display_(/*5.2*/flash/*5.7*/.get("success").getOrElse("")),format.raw/*5.36*/("""

"""),_display_(/*7.2*/form(routes.CartController.addHandle())/*7.41*/ {_display_(Seq[Any](format.raw/*7.43*/("""
    """),_display_(/*8.6*/helper/*8.12*/.CSRF.formField),format.raw/*8.27*/("""
    """),_display_(/*9.6*/inputText(cart("user_id"))),format.raw/*9.32*/("""
    """),_display_(/*10.6*/inputText(cart("product_id"))),format.raw/*10.35*/("""
    """),_display_(/*11.6*/inputText(cart("quantity"))),format.raw/*11.33*/("""

    """),format.raw/*13.5*/("""<div class="buttons">
        <input type="submit" value="Add to cart"/>
    </div>
""")))}))
      }
    }
  }

  def render(cart:Form[CreateCartForm],request:MessagesRequestHeader,flash:Flash): play.twirl.api.HtmlFormat.Appendable = apply(cart)(request,flash)

  def f:((Form[CreateCartForm]) => (MessagesRequestHeader,Flash) => play.twirl.api.HtmlFormat.Appendable) = (cart) => (request,flash) => apply(cart)(request,flash)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2020-09-12T00:26:32.955
                  SOURCE: /Users/nuxie/IdeaProjects/untitled12/app/views/cart/add.scala.html
                  HASH: 0c05129577b3c32b0a7aed20faf845c3eb9a42ec
                  MATRIX: 774->1|930->87|974->85|1001->103|1028->105|1040->110|1089->139|1117->142|1164->181|1203->183|1234->189|1248->195|1283->210|1314->216|1360->242|1392->248|1442->277|1474->283|1522->310|1555->316
                  LINES: 21->1|24->3|27->2|28->4|29->5|29->5|29->5|31->7|31->7|31->7|32->8|32->8|32->8|33->9|33->9|34->10|34->10|35->11|35->11|37->13
                  -- GENERATED --
              */
          