// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/nuxie/IdeaProjects/untitled12/conf/routes
// @DATE:Sat Sep 12 03:11:54 CEST 2020

import play.api.routing.JavaScriptReverseRoute


import _root_.controllers.Assets.Asset

// @LINE:5
package controllers.javascript {

  // @LINE:8
  class ReverseAssets(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:8
    def versioned: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Assets.versioned",
      """
        function(file1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[play.api.mvc.PathBindable[Asset]].javascriptUnbind + """)("file", file1)})
        }
      """
    )
  
  }

  // @LINE:141
  class ReverseRegisterController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:141
    def submit: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.RegisterController.submit",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "auth/register"})
        }
      """
    )
  
  }

  // @LINE:128
  class ReverseWishlistController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:136
    def details: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WishlistController.details",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "wishlists/details/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:137
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WishlistController.update",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "wishlists/update/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:138
    def updateHandle: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WishlistController.updateHandle",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "wishlists/update"})
        }
      """
    )
  
    // @LINE:131
    def updateJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WishlistController.updateJSON",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/wishlists/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:139
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WishlistController.delete",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "wishlists/delete/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:128
    def listJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WishlistController.listJSON",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/wishlists"})
        }
      """
    )
  
    // @LINE:132
    def deleteJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WishlistController.deleteJSON",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/wishlists/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:133
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WishlistController.add",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "wishlists/add"})
        }
      """
    )
  
    // @LINE:135
    def list: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WishlistController.list",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "wishlists/all"})
        }
      """
    )
  
    // @LINE:130
    def addJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WishlistController.addJSON",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/wishlists"})
        }
      """
    )
  
    // @LINE:129
    def detailsJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WishlistController.detailsJSON",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/wishlists/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:134
    def addHandle: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.WishlistController.addHandle",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "wishlists/addhandle"})
        }
      """
    )
  
  }

  // @LINE:88
  class ReversePromotionController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:96
    def details: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PromotionController.details",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "promotions/details/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:97
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PromotionController.update",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "promotions/update/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:98
    def updateHandle: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PromotionController.updateHandle",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "promotions/update"})
        }
      """
    )
  
    // @LINE:91
    def updateJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PromotionController.updateJSON",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/promotions/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:93
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PromotionController.create",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "promotions/create"})
        }
      """
    )
  
    // @LINE:94
    def createHandle: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PromotionController.createHandle",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "promotions/createhandle"})
        }
      """
    )
  
    // @LINE:88
    def listJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PromotionController.listJSON",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/promotions"})
        }
      """
    )
  
    // @LINE:92
    def deleteJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PromotionController.deleteJSON",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/promotions/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:95
    def list: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PromotionController.list",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "promotions/all"})
        }
      """
    )
  
    // @LINE:90
    def addJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PromotionController.addJSON",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/promotions"})
        }
      """
    )
  
    // @LINE:89
    def detailsJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.PromotionController.detailsJSON",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/promotions/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
  }

  // @LINE:115
  class ReverseStockController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:123
    def details: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.StockController.details",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "stocks/details/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:124
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.StockController.update",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "stocks/update/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:125
    def updateHandle: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.StockController.updateHandle",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "stocks/update"})
        }
      """
    )
  
    // @LINE:118
    def updateJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.StockController.updateJSON",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/stocks/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:126
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.StockController.delete",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "stocks/delete/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:115
    def listJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.StockController.listJSON",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/stocks"})
        }
      """
    )
  
    // @LINE:119
    def deleteJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.StockController.deleteJSON",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/stocks/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:120
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.StockController.add",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "stocks/add"})
        }
      """
    )
  
    // @LINE:122
    def list: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.StockController.list",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "stocks/all"})
        }
      """
    )
  
    // @LINE:117
    def addJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.StockController.addJSON",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/stocks"})
        }
      """
    )
  
    // @LINE:116
    def detailsJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.StockController.detailsJSON",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/stocks/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:121
    def addHandle: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.StockController.addHandle",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "stocks/addhandle"})
        }
      """
    )
  
  }

  // @LINE:34
  class ReverseInvoiceController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:42
    def details: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InvoiceController.details",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "invoices/details/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:43
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InvoiceController.update",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "invoices/update/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:44
    def updateHandle: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InvoiceController.updateHandle",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "invoices/update"})
        }
      """
    )
  
    // @LINE:37
    def updateJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InvoiceController.updateJSON",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/invoices/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:39
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InvoiceController.create",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "invoices/create"})
        }
      """
    )
  
    // @LINE:45
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InvoiceController.delete",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "invoices/delete/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:40
    def createHandle: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InvoiceController.createHandle",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "invoices/createhandle"})
        }
      """
    )
  
    // @LINE:34
    def listJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InvoiceController.listJSON",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/invoices"})
        }
      """
    )
  
    // @LINE:38
    def deleteJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InvoiceController.deleteJSON",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/invoices/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:36
    def createJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InvoiceController.createJSON",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/invoices"})
        }
      """
    )
  
    // @LINE:41
    def list: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InvoiceController.list",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "invoices/all"})
        }
      """
    )
  
    // @LINE:35
    def detailsJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.InvoiceController.detailsJSON",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/invoices/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
  }

  // @LINE:11
  class ReverseCartController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:15
    def updateJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CartController.updateJSON",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/cart/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:11
    def detailsUserJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CartController.detailsUserJSON",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/cart/details"})
        }
      """
    )
  
    // @LINE:13
    def detailsUserExtendedJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CartController.detailsUserExtendedJSON",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/cart/details_extended"})
        }
      """
    )
  
    // @LINE:16
    def deleteJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CartController.deleteJSON",
      """
        function(uid0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/cart/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("uid", uid0))})
        }
      """
    )
  
    // @LINE:12
    def deleteUser: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CartController.deleteUser",
      """
        function() {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/cart/delete"})
        }
      """
    )
  
    // @LINE:17
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CartController.add",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "cart/add"})
        }
      """
    )
  
    // @LINE:19
    def list: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CartController.list",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "cart/all"})
        }
      """
    )
  
    // @LINE:14
    def addJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CartController.addJSON",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/cart"})
        }
      """
    )
  
    // @LINE:18
    def addHandle: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CartController.addHandle",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "cart/addHandle"})
        }
      """
    )
  
  }

  // @LINE:21
  class ReverseCategoryController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:29
    def details: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CategoryController.details",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "categories/details/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:30
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CategoryController.update",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "categories/update/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:31
    def updateHandle: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CategoryController.updateHandle",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "categories/update"})
        }
      """
    )
  
    // @LINE:24
    def updateJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CategoryController.updateJSON",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/categories/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:26
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CategoryController.create",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "categories/create"})
        }
      """
    )
  
    // @LINE:32
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CategoryController.delete",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "categories/delete/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:27
    def createHandle: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CategoryController.createHandle",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "categories/createhandle"})
        }
      """
    )
  
    // @LINE:21
    def listJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CategoryController.listJSON",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/categories"})
        }
      """
    )
  
    // @LINE:25
    def deleteJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CategoryController.deleteJSON",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/categories/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:23
    def createJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CategoryController.createJSON",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/categories"})
        }
      """
    )
  
    // @LINE:28
    def list: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CategoryController.list",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "categories/all"})
        }
      """
    )
  
    // @LINE:22
    def detailsJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CategoryController.detailsJSON",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/categories/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
  }

  // @LINE:60
  class ReverseOrderProductsController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:68
    def details: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrderProductsController.details",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "order_products/details/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:69
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrderProductsController.update",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "order_products/update/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:70
    def updateHandle: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrderProductsController.updateHandle",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "order_products/update"})
        }
      """
    )
  
    // @LINE:63
    def updateJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrderProductsController.updateJSON",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/order_products/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:71
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrderProductsController.delete",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "order_products/delete/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:60
    def listJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrderProductsController.listJSON",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/order_products"})
        }
      """
    )
  
    // @LINE:64
    def deleteJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrderProductsController.deleteJSON",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/order_products/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:65
    def add: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrderProductsController.add",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "order_products/add"})
        }
      """
    )
  
    // @LINE:67
    def list: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrderProductsController.list",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "order_products/all"})
        }
      """
    )
  
    // @LINE:62
    def addJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrderProductsController.addJSON",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/order_products"})
        }
      """
    )
  
    // @LINE:61
    def detailsJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrderProductsController.detailsJSON",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/order_products/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:66
    def addHandle: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrderProductsController.addHandle",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "order_products/addHandle"})
        }
      """
    )
  
  }

  // @LINE:73
  class ReverseProductController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:83
    def details: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ProductController.details",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "products/details/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:84
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ProductController.update",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "products/update/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:85
    def updateHandle: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ProductController.updateHandle",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "products/update"})
        }
      """
    )
  
    // @LINE:77
    def updateJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ProductController.updateJSON",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/products/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:79
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ProductController.create",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "products/create"})
        }
      """
    )
  
    // @LINE:86
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ProductController.delete",
      """
        function(id0) {
        
          if (true) {
            return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "products/delete/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
          }
        
        }
      """
    )
  
    // @LINE:80
    def createHandle: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ProductController.createHandle",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "products/createhandle"})
        }
      """
    )
  
    // @LINE:74
    def extendedListJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ProductController.extendedListJSON",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/products/extended"})
        }
      """
    )
  
    // @LINE:82
    def extendedList: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ProductController.extendedList",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "products/all_extended"})
        }
      """
    )
  
    // @LINE:73
    def listJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ProductController.listJSON",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/products"})
        }
      """
    )
  
    // @LINE:78
    def deleteJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ProductController.deleteJSON",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/products/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:81
    def list: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ProductController.list",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "products/all"})
        }
      """
    )
  
    // @LINE:76
    def addJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ProductController.addJSON",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/products"})
        }
      """
    )
  
    // @LINE:75
    def detailsJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ProductController.detailsJSON",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/products/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
  }

  // @LINE:101
  class ReverseReviewController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:110
    def details: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ReviewController.details",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "reviews/details/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:111
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ReviewController.update",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "reviews/update/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:112
    def updateHandle: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ReviewController.updateHandle",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "reviews/update"})
        }
      """
    )
  
    // @LINE:105
    def updateJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ReviewController.updateJSON",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/reviews/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:102
    def listProductJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ReviewController.listProductJSON",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/product_reviews/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:107
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ReviewController.create",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "reviews/create"})
        }
      """
    )
  
    // @LINE:113
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ReviewController.delete",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "reviews/delete/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:108
    def createHandle: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ReviewController.createHandle",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "reviews/createhandle"})
        }
      """
    )
  
    // @LINE:101
    def listJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ReviewController.listJSON",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/reviews"})
        }
      """
    )
  
    // @LINE:106
    def deleteJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ReviewController.deleteJSON",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/reviews/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:104
    def createJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ReviewController.createJSON",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/reviews"})
        }
      """
    )
  
    // @LINE:109
    def list: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ReviewController.list",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "reviews/all"})
        }
      """
    )
  
    // @LINE:103
    def detailsJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ReviewController.detailsJSON",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/reviews/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
  }

  // @LINE:144
  class ReverseSocialProviderController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:144
    def authenticate: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SocialProviderController.authenticate",
      """
        function(provider0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "auth/provider/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("provider", provider0))})
        }
      """
    )
  
  }

  // @LINE:5
  class ReverseHomeController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:5
    def index: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.index",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + """"})
        }
      """
    )
  
  }

  // @LINE:142
  class ReverseLoginController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:143
    def signOut: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.LoginController.signOut",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "auth/logout"})
        }
      """
    )
  
    // @LINE:142
    def submit: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.LoginController.submit",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "auth/login"})
        }
      """
    )
  
  }

  // @LINE:47
  class ReverseOrderController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:55
    def details: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrderController.details",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "orders/details/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:56
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrderController.update",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "orders/update/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:57
    def updateHandle: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrderController.updateHandle",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "orders/update"})
        }
      """
    )
  
    // @LINE:50
    def updateJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrderController.updateJSON",
      """
        function(id0) {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/orders/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:52
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrderController.create",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "orders/create"})
        }
      """
    )
  
    // @LINE:58
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrderController.delete",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "orders/delete/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:53
    def createHandle: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrderController.createHandle",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "orders/createhandle"})
        }
      """
    )
  
    // @LINE:47
    def listJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrderController.listJSON",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/orders"})
        }
      """
    )
  
    // @LINE:51
    def deleteJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrderController.deleteJSON",
      """
        function(id0) {
          return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/orders/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:49
    def createJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrderController.createJSON",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/orders"})
        }
      """
    )
  
    // @LINE:54
    def list: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrderController.list",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "orders/all"})
        }
      """
    )
  
    // @LINE:48
    def detailsJSON: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.OrderController.detailsJSON",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/orders/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
  }


}
