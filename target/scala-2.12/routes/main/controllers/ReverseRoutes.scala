// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/nuxie/IdeaProjects/untitled12/conf/routes
// @DATE:Sat Sep 12 03:11:54 CEST 2020

import play.api.mvc.Call


import _root_.controllers.Assets.Asset

// @LINE:5
package controllers {

  // @LINE:8
  class ReverseAssets(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:8
    def versioned(file:Asset): Call = {
      implicit lazy val _rrc = new play.core.routing.ReverseRouteContext(Map(("path", "/public"))); _rrc
      Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[play.api.mvc.PathBindable[Asset]].unbind("file", file))
    }
  
  }

  // @LINE:141
  class ReverseRegisterController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:141
    def submit(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "auth/register")
    }
  
  }

  // @LINE:128
  class ReverseWishlistController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:136
    def details(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "wishlists/details/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:137
    def update(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "wishlists/update/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:138
    def updateHandle(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "wishlists/update")
    }
  
    // @LINE:131
    def updateJSON(id:Int): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/wishlists/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:139
    def delete(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "wishlists/delete/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:128
    def listJSON(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/wishlists")
    }
  
    // @LINE:132
    def deleteJSON(id:Int): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/wishlists/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:133
    def add(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "wishlists/add")
    }
  
    // @LINE:135
    def list(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "wishlists/all")
    }
  
    // @LINE:130
    def addJSON(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/wishlists")
    }
  
    // @LINE:129
    def detailsJSON(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/wishlists/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:134
    def addHandle(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "wishlists/addhandle")
    }
  
  }

  // @LINE:88
  class ReversePromotionController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:96
    def details(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "promotions/details/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:97
    def update(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "promotions/update/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:98
    def updateHandle(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "promotions/update")
    }
  
    // @LINE:91
    def updateJSON(id:Int): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/promotions/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:93
    def create(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "promotions/create")
    }
  
    // @LINE:94
    def createHandle(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "promotions/createhandle")
    }
  
    // @LINE:88
    def listJSON(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/promotions")
    }
  
    // @LINE:92
    def deleteJSON(id:Int): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/promotions/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:95
    def list(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "promotions/all")
    }
  
    // @LINE:90
    def addJSON(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/promotions")
    }
  
    // @LINE:89
    def detailsJSON(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/promotions/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
  }

  // @LINE:115
  class ReverseStockController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:123
    def details(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "stocks/details/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:124
    def update(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "stocks/update/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:125
    def updateHandle(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "stocks/update")
    }
  
    // @LINE:118
    def updateJSON(id:Int): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/stocks/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:126
    def delete(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "stocks/delete/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:115
    def listJSON(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/stocks")
    }
  
    // @LINE:119
    def deleteJSON(id:Int): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/stocks/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:120
    def add(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "stocks/add")
    }
  
    // @LINE:122
    def list(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "stocks/all")
    }
  
    // @LINE:117
    def addJSON(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/stocks")
    }
  
    // @LINE:116
    def detailsJSON(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/stocks/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:121
    def addHandle(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "stocks/addhandle")
    }
  
  }

  // @LINE:34
  class ReverseInvoiceController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:42
    def details(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "invoices/details/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:43
    def update(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "invoices/update/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:44
    def updateHandle(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "invoices/update")
    }
  
    // @LINE:37
    def updateJSON(id:Int): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/invoices/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:39
    def create(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "invoices/create")
    }
  
    // @LINE:45
    def delete(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "invoices/delete/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:40
    def createHandle(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "invoices/createhandle")
    }
  
    // @LINE:34
    def listJSON(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/invoices")
    }
  
    // @LINE:38
    def deleteJSON(id:Int): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/invoices/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:36
    def createJSON(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/invoices")
    }
  
    // @LINE:41
    def list(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "invoices/all")
    }
  
    // @LINE:35
    def detailsJSON(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/invoices/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
  }

  // @LINE:11
  class ReverseCartController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:15
    def updateJSON(id:Int): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/cart/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:11
    def detailsUserJSON(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/cart/details")
    }
  
    // @LINE:13
    def detailsUserExtendedJSON(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/cart/details_extended")
    }
  
    // @LINE:16
    def deleteJSON(uid:String): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/cart/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("uid", uid)))
    }
  
    // @LINE:12
    def deleteUser(): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/cart/delete")
    }
  
    // @LINE:17
    def add(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "cart/add")
    }
  
    // @LINE:19
    def list(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "cart/all")
    }
  
    // @LINE:14
    def addJSON(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/cart")
    }
  
    // @LINE:18
    def addHandle(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "cart/addHandle")
    }
  
  }

  // @LINE:21
  class ReverseCategoryController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:29
    def details(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "categories/details/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:30
    def update(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "categories/update/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:31
    def updateHandle(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "categories/update")
    }
  
    // @LINE:24
    def updateJSON(id:Int): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/categories/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:26
    def create(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "categories/create")
    }
  
    // @LINE:32
    def delete(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "categories/delete/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:27
    def createHandle(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "categories/createhandle")
    }
  
    // @LINE:21
    def listJSON(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/categories")
    }
  
    // @LINE:25
    def deleteJSON(id:Int): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/categories/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:23
    def createJSON(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/categories")
    }
  
    // @LINE:28
    def list(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "categories/all")
    }
  
    // @LINE:22
    def detailsJSON(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/categories/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
  }

  // @LINE:60
  class ReverseOrderProductsController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:68
    def details(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "order_products/details/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:69
    def update(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "order_products/update/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:70
    def updateHandle(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "order_products/update")
    }
  
    // @LINE:63
    def updateJSON(id:Int): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/order_products/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:71
    def delete(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "order_products/delete/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:60
    def listJSON(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/order_products")
    }
  
    // @LINE:64
    def deleteJSON(id:Int): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/order_products/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:65
    def add(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "order_products/add")
    }
  
    // @LINE:67
    def list(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "order_products/all")
    }
  
    // @LINE:62
    def addJSON(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/order_products")
    }
  
    // @LINE:61
    def detailsJSON(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/order_products/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:66
    def addHandle(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "order_products/addHandle")
    }
  
  }

  // @LINE:73
  class ReverseProductController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:83
    def details(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "products/details/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:84
    def update(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "products/update/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:85
    def updateHandle(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "products/update")
    }
  
    // @LINE:77
    def updateJSON(id:Int): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/products/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:79
    def create(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "products/create")
    }
  
    // @LINE:86
    def delete(id:Int): Call = {
    
      (id: @unchecked) match {
      
        // @LINE:86
        case (id)  =>
          
          Call("GET", _prefix + { _defaultPrefix } + "products/delete/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
      
      }
    
    }
  
    // @LINE:80
    def createHandle(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "products/createhandle")
    }
  
    // @LINE:74
    def extendedListJSON(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/products/extended")
    }
  
    // @LINE:82
    def extendedList(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "products/all_extended")
    }
  
    // @LINE:73
    def listJSON(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/products")
    }
  
    // @LINE:78
    def deleteJSON(id:Int): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/products/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:81
    def list(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "products/all")
    }
  
    // @LINE:76
    def addJSON(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/products")
    }
  
    // @LINE:75
    def detailsJSON(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/products/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
  }

  // @LINE:101
  class ReverseReviewController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:110
    def details(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "reviews/details/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:111
    def update(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "reviews/update/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:112
    def updateHandle(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "reviews/update")
    }
  
    // @LINE:105
    def updateJSON(id:Int): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/reviews/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:102
    def listProductJSON(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/product_reviews/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:107
    def create(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "reviews/create")
    }
  
    // @LINE:113
    def delete(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "reviews/delete/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:108
    def createHandle(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "reviews/createhandle")
    }
  
    // @LINE:101
    def listJSON(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/reviews")
    }
  
    // @LINE:106
    def deleteJSON(id:Int): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/reviews/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:104
    def createJSON(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/reviews")
    }
  
    // @LINE:109
    def list(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "reviews/all")
    }
  
    // @LINE:103
    def detailsJSON(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/reviews/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
  }

  // @LINE:144
  class ReverseSocialProviderController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:144
    def authenticate(provider:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "auth/provider/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("provider", provider)))
    }
  
  }

  // @LINE:5
  class ReverseHomeController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:5
    def index(): Call = {
      
      Call("GET", _prefix)
    }
  
  }

  // @LINE:142
  class ReverseLoginController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:143
    def signOut(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "auth/logout")
    }
  
    // @LINE:142
    def submit(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "auth/login")
    }
  
  }

  // @LINE:47
  class ReverseOrderController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:55
    def details(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "orders/details/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:56
    def update(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "orders/update/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:57
    def updateHandle(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "orders/update")
    }
  
    // @LINE:50
    def updateJSON(id:Int): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "api/orders/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:52
    def create(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "orders/create")
    }
  
    // @LINE:58
    def delete(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "orders/delete/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:53
    def createHandle(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "orders/createhandle")
    }
  
    // @LINE:47
    def listJSON(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/orders")
    }
  
    // @LINE:51
    def deleteJSON(id:Int): Call = {
      
      Call("DELETE", _prefix + { _defaultPrefix } + "api/orders/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:49
    def createJSON(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "api/orders")
    }
  
    // @LINE:54
    def list(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "orders/all")
    }
  
    // @LINE:48
    def detailsJSON(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "api/orders/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
  }


}
