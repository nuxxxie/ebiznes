// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/nuxie/IdeaProjects/untitled12/conf/routes
// @DATE:Sat Sep 12 03:11:54 CEST 2020

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseRegisterController RegisterController = new controllers.ReverseRegisterController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseWishlistController WishlistController = new controllers.ReverseWishlistController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReversePromotionController PromotionController = new controllers.ReversePromotionController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseStockController StockController = new controllers.ReverseStockController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseInvoiceController InvoiceController = new controllers.ReverseInvoiceController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseCartController CartController = new controllers.ReverseCartController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseCategoryController CategoryController = new controllers.ReverseCategoryController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseOrderProductsController OrderProductsController = new controllers.ReverseOrderProductsController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseProductController ProductController = new controllers.ReverseProductController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseReviewController ReviewController = new controllers.ReverseReviewController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseSocialProviderController SocialProviderController = new controllers.ReverseSocialProviderController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseHomeController HomeController = new controllers.ReverseHomeController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseLoginController LoginController = new controllers.ReverseLoginController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseOrderController OrderController = new controllers.ReverseOrderController(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseRegisterController RegisterController = new controllers.javascript.ReverseRegisterController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseWishlistController WishlistController = new controllers.javascript.ReverseWishlistController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReversePromotionController PromotionController = new controllers.javascript.ReversePromotionController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseStockController StockController = new controllers.javascript.ReverseStockController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseInvoiceController InvoiceController = new controllers.javascript.ReverseInvoiceController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseCartController CartController = new controllers.javascript.ReverseCartController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseCategoryController CategoryController = new controllers.javascript.ReverseCategoryController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseOrderProductsController OrderProductsController = new controllers.javascript.ReverseOrderProductsController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseProductController ProductController = new controllers.javascript.ReverseProductController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseReviewController ReviewController = new controllers.javascript.ReverseReviewController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseSocialProviderController SocialProviderController = new controllers.javascript.ReverseSocialProviderController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseHomeController HomeController = new controllers.javascript.ReverseHomeController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseLoginController LoginController = new controllers.javascript.ReverseLoginController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseOrderController OrderController = new controllers.javascript.ReverseOrderController(RoutesPrefix.byNamePrefix());
  }

}
