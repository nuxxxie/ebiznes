// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/nuxie/IdeaProjects/untitled12/conf/routes
// @DATE:Sat Sep 12 03:11:54 CEST 2020

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:5
  HomeController_14: controllers.HomeController,
  // @LINE:8
  Assets_11: controllers.Assets,
  // @LINE:11
  CartController_0: controllers.CartController,
  // @LINE:21
  CategoryController_6: controllers.CategoryController,
  // @LINE:34
  InvoiceController_5: controllers.InvoiceController,
  // @LINE:47
  OrderController_1: controllers.OrderController,
  // @LINE:60
  OrderProductsController_2: controllers.OrderProductsController,
  // @LINE:73
  ProductController_9: controllers.ProductController,
  // @LINE:88
  PromotionController_7: controllers.PromotionController,
  // @LINE:101
  ReviewController_3: controllers.ReviewController,
  // @LINE:115
  StockController_8: controllers.StockController,
  // @LINE:128
  WishlistController_12: controllers.WishlistController,
  // @LINE:141
  RegisterController_13: controllers.RegisterController,
  // @LINE:142
  LoginController_4: controllers.LoginController,
  // @LINE:144
  SocialProviderController_10: controllers.SocialProviderController,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:5
    HomeController_14: controllers.HomeController,
    // @LINE:8
    Assets_11: controllers.Assets,
    // @LINE:11
    CartController_0: controllers.CartController,
    // @LINE:21
    CategoryController_6: controllers.CategoryController,
    // @LINE:34
    InvoiceController_5: controllers.InvoiceController,
    // @LINE:47
    OrderController_1: controllers.OrderController,
    // @LINE:60
    OrderProductsController_2: controllers.OrderProductsController,
    // @LINE:73
    ProductController_9: controllers.ProductController,
    // @LINE:88
    PromotionController_7: controllers.PromotionController,
    // @LINE:101
    ReviewController_3: controllers.ReviewController,
    // @LINE:115
    StockController_8: controllers.StockController,
    // @LINE:128
    WishlistController_12: controllers.WishlistController,
    // @LINE:141
    RegisterController_13: controllers.RegisterController,
    // @LINE:142
    LoginController_4: controllers.LoginController,
    // @LINE:144
    SocialProviderController_10: controllers.SocialProviderController
  ) = this(errorHandler, HomeController_14, Assets_11, CartController_0, CategoryController_6, InvoiceController_5, OrderController_1, OrderProductsController_2, ProductController_9, PromotionController_7, ReviewController_3, StockController_8, WishlistController_12, RegisterController_13, LoginController_4, SocialProviderController_10, "/")

  def withPrefix(addPrefix: String): Routes = {
    val prefix = play.api.routing.Router.concatPrefix(addPrefix, this.prefix)
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, HomeController_14, Assets_11, CartController_0, CategoryController_6, InvoiceController_5, OrderController_1, OrderProductsController_2, ProductController_9, PromotionController_7, ReviewController_3, StockController_8, WishlistController_12, RegisterController_13, LoginController_4, SocialProviderController_10, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.HomeController.index"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/cart/details""", """controllers.CartController.detailsUserJSON()"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/cart/delete""", """controllers.CartController.deleteUser()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/cart/details_extended""", """controllers.CartController.detailsUserExtendedJSON()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/cart""", """controllers.CartController.addJSON()"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/cart/""" + "$" + """id<[^/]+>""", """controllers.CartController.updateJSON(id:Int)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/cart/""" + "$" + """uid<[^/]+>""", """controllers.CartController.deleteJSON(uid:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """cart/add""", """controllers.CartController.add"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """cart/addHandle""", """controllers.CartController.addHandle"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """cart/all""", """controllers.CartController.list"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/categories""", """controllers.CategoryController.listJSON()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/categories/""" + "$" + """id<[^/]+>""", """controllers.CategoryController.detailsJSON(id:Int)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/categories""", """controllers.CategoryController.createJSON()"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/categories/""" + "$" + """id<[^/]+>""", """controllers.CategoryController.updateJSON(id:Int)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/categories/""" + "$" + """id<[^/]+>""", """controllers.CategoryController.deleteJSON(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """categories/create""", """controllers.CategoryController.create"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """categories/createhandle""", """controllers.CategoryController.createHandle"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """categories/all""", """controllers.CategoryController.list"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """categories/details/""" + "$" + """id<[^/]+>""", """controllers.CategoryController.details(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """categories/update/""" + "$" + """id<[^/]+>""", """controllers.CategoryController.update(id:Int)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """categories/update""", """controllers.CategoryController.updateHandle"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """categories/delete/""" + "$" + """id<[^/]+>""", """controllers.CategoryController.delete(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/invoices""", """controllers.InvoiceController.listJSON()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/invoices/""" + "$" + """id<[^/]+>""", """controllers.InvoiceController.detailsJSON(id:Int)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/invoices""", """controllers.InvoiceController.createJSON()"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/invoices/""" + "$" + """id<[^/]+>""", """controllers.InvoiceController.updateJSON(id:Int)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/invoices/""" + "$" + """id<[^/]+>""", """controllers.InvoiceController.deleteJSON(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """invoices/create""", """controllers.InvoiceController.create"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """invoices/createhandle""", """controllers.InvoiceController.createHandle"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """invoices/all""", """controllers.InvoiceController.list"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """invoices/details/""" + "$" + """id<[^/]+>""", """controllers.InvoiceController.details(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """invoices/update/""" + "$" + """id<[^/]+>""", """controllers.InvoiceController.update(id:Int)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """invoices/update""", """controllers.InvoiceController.updateHandle"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """invoices/delete/""" + "$" + """id<[^/]+>""", """controllers.InvoiceController.delete(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/orders""", """controllers.OrderController.listJSON()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/orders/""" + "$" + """id<[^/]+>""", """controllers.OrderController.detailsJSON(id:Int)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/orders""", """controllers.OrderController.createJSON()"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/orders/""" + "$" + """id<[^/]+>""", """controllers.OrderController.updateJSON(id:Int)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/orders/""" + "$" + """id<[^/]+>""", """controllers.OrderController.deleteJSON(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders/create""", """controllers.OrderController.create"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders/createhandle""", """controllers.OrderController.createHandle"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders/all""", """controllers.OrderController.list"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders/details/""" + "$" + """id<[^/]+>""", """controllers.OrderController.details(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders/update/""" + "$" + """id<[^/]+>""", """controllers.OrderController.update(id:Int)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders/update""", """controllers.OrderController.updateHandle"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """orders/delete/""" + "$" + """id<[^/]+>""", """controllers.OrderController.delete(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/order_products""", """controllers.OrderProductsController.listJSON()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/order_products/""" + "$" + """id<[^/]+>""", """controllers.OrderProductsController.detailsJSON(id:Int)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/order_products""", """controllers.OrderProductsController.addJSON()"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/order_products/""" + "$" + """id<[^/]+>""", """controllers.OrderProductsController.updateJSON(id:Int)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/order_products/""" + "$" + """id<[^/]+>""", """controllers.OrderProductsController.deleteJSON(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """order_products/add""", """controllers.OrderProductsController.add"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """order_products/addHandle""", """controllers.OrderProductsController.addHandle"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """order_products/all""", """controllers.OrderProductsController.list"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """order_products/details/""" + "$" + """id<[^/]+>""", """controllers.OrderProductsController.details(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """order_products/update/""" + "$" + """id<[^/]+>""", """controllers.OrderProductsController.update(id:Int)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """order_products/update""", """controllers.OrderProductsController.updateHandle"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """order_products/delete/""" + "$" + """id<[^/]+>""", """controllers.OrderProductsController.delete(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/products""", """controllers.ProductController.listJSON()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/products/extended""", """controllers.ProductController.extendedListJSON()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/products/""" + "$" + """id<[^/]+>""", """controllers.ProductController.detailsJSON(id:Int)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/products""", """controllers.ProductController.addJSON()"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/products/""" + "$" + """id<[^/]+>""", """controllers.ProductController.updateJSON(id:Int)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/products/""" + "$" + """id<[^/]+>""", """controllers.ProductController.deleteJSON(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/create""", """controllers.ProductController.create"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/createhandle""", """controllers.ProductController.createHandle"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/all""", """controllers.ProductController.list"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/all_extended""", """controllers.ProductController.extendedList"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/details/""" + "$" + """id<[^/]+>""", """controllers.ProductController.details(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/update/""" + "$" + """id<[^/]+>""", """controllers.ProductController.update(id:Int)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/update""", """controllers.ProductController.updateHandle"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """products/delete/""" + "$" + """id<[^/]+>""", """controllers.ProductController.delete(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/promotions""", """controllers.PromotionController.listJSON()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/promotions/""" + "$" + """id<[^/]+>""", """controllers.PromotionController.detailsJSON(id:Int)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/promotions""", """controllers.PromotionController.addJSON()"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/promotions/""" + "$" + """id<[^/]+>""", """controllers.PromotionController.updateJSON(id:Int)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/promotions/""" + "$" + """id<[^/]+>""", """controllers.PromotionController.deleteJSON(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """promotions/create""", """controllers.PromotionController.create"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """promotions/createhandle""", """controllers.PromotionController.createHandle"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """promotions/all""", """controllers.PromotionController.list"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """promotions/details/""" + "$" + """id<[^/]+>""", """controllers.PromotionController.details(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """promotions/update/""" + "$" + """id<[^/]+>""", """controllers.PromotionController.update(id:Int)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """promotions/update""", """controllers.PromotionController.updateHandle"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """promotions/delete/""" + "$" + """id<[^/]+>""", """controllers.ProductController.delete(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/reviews""", """controllers.ReviewController.listJSON()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/product_reviews/""" + "$" + """id<[^/]+>""", """controllers.ReviewController.listProductJSON(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/reviews/""" + "$" + """id<[^/]+>""", """controllers.ReviewController.detailsJSON(id:Int)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/reviews""", """controllers.ReviewController.createJSON()"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/reviews/""" + "$" + """id<[^/]+>""", """controllers.ReviewController.updateJSON(id:Int)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/reviews/""" + "$" + """id<[^/]+>""", """controllers.ReviewController.deleteJSON(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """reviews/create""", """controllers.ReviewController.create"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """reviews/createhandle""", """controllers.ReviewController.createHandle"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """reviews/all""", """controllers.ReviewController.list"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """reviews/details/""" + "$" + """id<[^/]+>""", """controllers.ReviewController.details(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """reviews/update/""" + "$" + """id<[^/]+>""", """controllers.ReviewController.update(id:Int)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """reviews/update""", """controllers.ReviewController.updateHandle"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """reviews/delete/""" + "$" + """id<[^/]+>""", """controllers.ReviewController.delete(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/stocks""", """controllers.StockController.listJSON()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/stocks/""" + "$" + """id<[^/]+>""", """controllers.StockController.detailsJSON(id:Int)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/stocks""", """controllers.StockController.addJSON()"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/stocks/""" + "$" + """id<[^/]+>""", """controllers.StockController.updateJSON(id:Int)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/stocks/""" + "$" + """id<[^/]+>""", """controllers.StockController.deleteJSON(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """stocks/add""", """controllers.StockController.add"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """stocks/addhandle""", """controllers.StockController.addHandle"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """stocks/all""", """controllers.StockController.list"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """stocks/details/""" + "$" + """id<[^/]+>""", """controllers.StockController.details(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """stocks/update/""" + "$" + """id<[^/]+>""", """controllers.StockController.update(id:Int)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """stocks/update""", """controllers.StockController.updateHandle"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """stocks/delete/""" + "$" + """id<[^/]+>""", """controllers.StockController.delete(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/wishlists""", """controllers.WishlistController.listJSON()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/wishlists/""" + "$" + """id<[^/]+>""", """controllers.WishlistController.detailsJSON(id:Int)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/wishlists""", """controllers.WishlistController.addJSON()"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/wishlists/""" + "$" + """id<[^/]+>""", """controllers.WishlistController.updateJSON(id:Int)"""),
    ("""DELETE""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/wishlists/""" + "$" + """id<[^/]+>""", """controllers.WishlistController.deleteJSON(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """wishlists/add""", """controllers.WishlistController.add"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """wishlists/addhandle""", """controllers.WishlistController.addHandle"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """wishlists/all""", """controllers.WishlistController.list"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """wishlists/details/""" + "$" + """id<[^/]+>""", """controllers.WishlistController.details(id:Int)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """wishlists/update/""" + "$" + """id<[^/]+>""", """controllers.WishlistController.update(id:Int)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """wishlists/update""", """controllers.WishlistController.updateHandle"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """wishlists/delete/""" + "$" + """id<[^/]+>""", """controllers.WishlistController.delete(id:Int)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """auth/register""", """controllers.RegisterController.submit"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """auth/login""", """controllers.LoginController.submit"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """auth/logout""", """controllers.LoginController.signOut"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """auth/provider/""" + "$" + """provider<[^/]+>""", """controllers.SocialProviderController.authenticate(provider:String)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:5
  private[this] lazy val controllers_HomeController_index0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_HomeController_index0_invoker = createInvoker(
    HomeController_14.index,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "index",
      Nil,
      "GET",
      this.prefix + """""",
      """""",
      Seq()
    )
  )

  // @LINE:8
  private[this] lazy val controllers_Assets_versioned1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned1_invoker = createInvoker(
    Assets_11.versioned(fakeValue[String], fakeValue[Asset]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      this.prefix + """assets/""" + "$" + """file<.+>""",
      """ Map static resources from the /public folder to the /assets URL path""",
      Seq()
    )
  )

  // @LINE:11
  private[this] lazy val controllers_CartController_detailsUserJSON2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/cart/details")))
  )
  private[this] lazy val controllers_CartController_detailsUserJSON2_invoker = createInvoker(
    CartController_0.detailsUserJSON(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CartController",
      "detailsUserJSON",
      Nil,
      "GET",
      this.prefix + """api/cart/details""",
      """GET         /api/cart                          controllers.CartController.listJSON()""",
      Seq()
    )
  )

  // @LINE:12
  private[this] lazy val controllers_CartController_deleteUser3_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/cart/delete")))
  )
  private[this] lazy val controllers_CartController_deleteUser3_invoker = createInvoker(
    CartController_0.deleteUser(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CartController",
      "deleteUser",
      Nil,
      "DELETE",
      this.prefix + """api/cart/delete""",
      """""",
      Seq()
    )
  )

  // @LINE:13
  private[this] lazy val controllers_CartController_detailsUserExtendedJSON4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/cart/details_extended")))
  )
  private[this] lazy val controllers_CartController_detailsUserExtendedJSON4_invoker = createInvoker(
    CartController_0.detailsUserExtendedJSON(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CartController",
      "detailsUserExtendedJSON",
      Nil,
      "GET",
      this.prefix + """api/cart/details_extended""",
      """""",
      Seq()
    )
  )

  // @LINE:14
  private[this] lazy val controllers_CartController_addJSON5_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/cart")))
  )
  private[this] lazy val controllers_CartController_addJSON5_invoker = createInvoker(
    CartController_0.addJSON(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CartController",
      "addJSON",
      Nil,
      "POST",
      this.prefix + """api/cart""",
      """""",
      Seq()
    )
  )

  // @LINE:15
  private[this] lazy val controllers_CartController_updateJSON6_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/cart/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CartController_updateJSON6_invoker = createInvoker(
    CartController_0.updateJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CartController",
      "updateJSON",
      Seq(classOf[Int]),
      "PUT",
      this.prefix + """api/cart/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:16
  private[this] lazy val controllers_CartController_deleteJSON7_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/cart/"), DynamicPart("uid", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CartController_deleteJSON7_invoker = createInvoker(
    CartController_0.deleteJSON(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CartController",
      "deleteJSON",
      Seq(classOf[String]),
      "DELETE",
      this.prefix + """api/cart/""" + "$" + """uid<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:17
  private[this] lazy val controllers_CartController_add8_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("cart/add")))
  )
  private[this] lazy val controllers_CartController_add8_invoker = createInvoker(
    CartController_0.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CartController",
      "add",
      Nil,
      "GET",
      this.prefix + """cart/add""",
      """""",
      Seq()
    )
  )

  // @LINE:18
  private[this] lazy val controllers_CartController_addHandle9_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("cart/addHandle")))
  )
  private[this] lazy val controllers_CartController_addHandle9_invoker = createInvoker(
    CartController_0.addHandle,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CartController",
      "addHandle",
      Nil,
      "POST",
      this.prefix + """cart/addHandle""",
      """""",
      Seq()
    )
  )

  // @LINE:19
  private[this] lazy val controllers_CartController_list10_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("cart/all")))
  )
  private[this] lazy val controllers_CartController_list10_invoker = createInvoker(
    CartController_0.list,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CartController",
      "list",
      Nil,
      "GET",
      this.prefix + """cart/all""",
      """""",
      Seq()
    )
  )

  // @LINE:21
  private[this] lazy val controllers_CategoryController_listJSON11_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/categories")))
  )
  private[this] lazy val controllers_CategoryController_listJSON11_invoker = createInvoker(
    CategoryController_6.listJSON(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CategoryController",
      "listJSON",
      Nil,
      "GET",
      this.prefix + """api/categories""",
      """""",
      Seq()
    )
  )

  // @LINE:22
  private[this] lazy val controllers_CategoryController_detailsJSON12_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/categories/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CategoryController_detailsJSON12_invoker = createInvoker(
    CategoryController_6.detailsJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CategoryController",
      "detailsJSON",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """api/categories/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:23
  private[this] lazy val controllers_CategoryController_createJSON13_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/categories")))
  )
  private[this] lazy val controllers_CategoryController_createJSON13_invoker = createInvoker(
    CategoryController_6.createJSON(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CategoryController",
      "createJSON",
      Nil,
      "POST",
      this.prefix + """api/categories""",
      """""",
      Seq()
    )
  )

  // @LINE:24
  private[this] lazy val controllers_CategoryController_updateJSON14_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/categories/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CategoryController_updateJSON14_invoker = createInvoker(
    CategoryController_6.updateJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CategoryController",
      "updateJSON",
      Seq(classOf[Int]),
      "PUT",
      this.prefix + """api/categories/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:25
  private[this] lazy val controllers_CategoryController_deleteJSON15_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/categories/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CategoryController_deleteJSON15_invoker = createInvoker(
    CategoryController_6.deleteJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CategoryController",
      "deleteJSON",
      Seq(classOf[Int]),
      "DELETE",
      this.prefix + """api/categories/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:26
  private[this] lazy val controllers_CategoryController_create16_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("categories/create")))
  )
  private[this] lazy val controllers_CategoryController_create16_invoker = createInvoker(
    CategoryController_6.create,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CategoryController",
      "create",
      Nil,
      "GET",
      this.prefix + """categories/create""",
      """""",
      Seq()
    )
  )

  // @LINE:27
  private[this] lazy val controllers_CategoryController_createHandle17_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("categories/createhandle")))
  )
  private[this] lazy val controllers_CategoryController_createHandle17_invoker = createInvoker(
    CategoryController_6.createHandle,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CategoryController",
      "createHandle",
      Nil,
      "POST",
      this.prefix + """categories/createhandle""",
      """""",
      Seq()
    )
  )

  // @LINE:28
  private[this] lazy val controllers_CategoryController_list18_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("categories/all")))
  )
  private[this] lazy val controllers_CategoryController_list18_invoker = createInvoker(
    CategoryController_6.list,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CategoryController",
      "list",
      Nil,
      "GET",
      this.prefix + """categories/all""",
      """""",
      Seq()
    )
  )

  // @LINE:29
  private[this] lazy val controllers_CategoryController_details19_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("categories/details/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CategoryController_details19_invoker = createInvoker(
    CategoryController_6.details(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CategoryController",
      "details",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """categories/details/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:30
  private[this] lazy val controllers_CategoryController_update20_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("categories/update/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CategoryController_update20_invoker = createInvoker(
    CategoryController_6.update(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CategoryController",
      "update",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """categories/update/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:31
  private[this] lazy val controllers_CategoryController_updateHandle21_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("categories/update")))
  )
  private[this] lazy val controllers_CategoryController_updateHandle21_invoker = createInvoker(
    CategoryController_6.updateHandle,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CategoryController",
      "updateHandle",
      Nil,
      "POST",
      this.prefix + """categories/update""",
      """""",
      Seq()
    )
  )

  // @LINE:32
  private[this] lazy val controllers_CategoryController_delete22_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("categories/delete/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_CategoryController_delete22_invoker = createInvoker(
    CategoryController_6.delete(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CategoryController",
      "delete",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """categories/delete/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:34
  private[this] lazy val controllers_InvoiceController_listJSON23_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/invoices")))
  )
  private[this] lazy val controllers_InvoiceController_listJSON23_invoker = createInvoker(
    InvoiceController_5.listJSON(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InvoiceController",
      "listJSON",
      Nil,
      "GET",
      this.prefix + """api/invoices""",
      """""",
      Seq()
    )
  )

  // @LINE:35
  private[this] lazy val controllers_InvoiceController_detailsJSON24_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/invoices/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_InvoiceController_detailsJSON24_invoker = createInvoker(
    InvoiceController_5.detailsJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InvoiceController",
      "detailsJSON",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """api/invoices/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:36
  private[this] lazy val controllers_InvoiceController_createJSON25_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/invoices")))
  )
  private[this] lazy val controllers_InvoiceController_createJSON25_invoker = createInvoker(
    InvoiceController_5.createJSON(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InvoiceController",
      "createJSON",
      Nil,
      "POST",
      this.prefix + """api/invoices""",
      """""",
      Seq()
    )
  )

  // @LINE:37
  private[this] lazy val controllers_InvoiceController_updateJSON26_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/invoices/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_InvoiceController_updateJSON26_invoker = createInvoker(
    InvoiceController_5.updateJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InvoiceController",
      "updateJSON",
      Seq(classOf[Int]),
      "PUT",
      this.prefix + """api/invoices/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:38
  private[this] lazy val controllers_InvoiceController_deleteJSON27_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/invoices/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_InvoiceController_deleteJSON27_invoker = createInvoker(
    InvoiceController_5.deleteJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InvoiceController",
      "deleteJSON",
      Seq(classOf[Int]),
      "DELETE",
      this.prefix + """api/invoices/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:39
  private[this] lazy val controllers_InvoiceController_create28_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("invoices/create")))
  )
  private[this] lazy val controllers_InvoiceController_create28_invoker = createInvoker(
    InvoiceController_5.create,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InvoiceController",
      "create",
      Nil,
      "GET",
      this.prefix + """invoices/create""",
      """""",
      Seq()
    )
  )

  // @LINE:40
  private[this] lazy val controllers_InvoiceController_createHandle29_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("invoices/createhandle")))
  )
  private[this] lazy val controllers_InvoiceController_createHandle29_invoker = createInvoker(
    InvoiceController_5.createHandle,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InvoiceController",
      "createHandle",
      Nil,
      "POST",
      this.prefix + """invoices/createhandle""",
      """""",
      Seq()
    )
  )

  // @LINE:41
  private[this] lazy val controllers_InvoiceController_list30_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("invoices/all")))
  )
  private[this] lazy val controllers_InvoiceController_list30_invoker = createInvoker(
    InvoiceController_5.list,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InvoiceController",
      "list",
      Nil,
      "GET",
      this.prefix + """invoices/all""",
      """""",
      Seq()
    )
  )

  // @LINE:42
  private[this] lazy val controllers_InvoiceController_details31_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("invoices/details/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_InvoiceController_details31_invoker = createInvoker(
    InvoiceController_5.details(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InvoiceController",
      "details",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """invoices/details/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:43
  private[this] lazy val controllers_InvoiceController_update32_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("invoices/update/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_InvoiceController_update32_invoker = createInvoker(
    InvoiceController_5.update(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InvoiceController",
      "update",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """invoices/update/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:44
  private[this] lazy val controllers_InvoiceController_updateHandle33_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("invoices/update")))
  )
  private[this] lazy val controllers_InvoiceController_updateHandle33_invoker = createInvoker(
    InvoiceController_5.updateHandle,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InvoiceController",
      "updateHandle",
      Nil,
      "POST",
      this.prefix + """invoices/update""",
      """""",
      Seq()
    )
  )

  // @LINE:45
  private[this] lazy val controllers_InvoiceController_delete34_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("invoices/delete/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_InvoiceController_delete34_invoker = createInvoker(
    InvoiceController_5.delete(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.InvoiceController",
      "delete",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """invoices/delete/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:47
  private[this] lazy val controllers_OrderController_listJSON35_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/orders")))
  )
  private[this] lazy val controllers_OrderController_listJSON35_invoker = createInvoker(
    OrderController_1.listJSON(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrderController",
      "listJSON",
      Nil,
      "GET",
      this.prefix + """api/orders""",
      """""",
      Seq()
    )
  )

  // @LINE:48
  private[this] lazy val controllers_OrderController_detailsJSON36_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/orders/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_OrderController_detailsJSON36_invoker = createInvoker(
    OrderController_1.detailsJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrderController",
      "detailsJSON",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """api/orders/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:49
  private[this] lazy val controllers_OrderController_createJSON37_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/orders")))
  )
  private[this] lazy val controllers_OrderController_createJSON37_invoker = createInvoker(
    OrderController_1.createJSON(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrderController",
      "createJSON",
      Nil,
      "POST",
      this.prefix + """api/orders""",
      """""",
      Seq()
    )
  )

  // @LINE:50
  private[this] lazy val controllers_OrderController_updateJSON38_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/orders/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_OrderController_updateJSON38_invoker = createInvoker(
    OrderController_1.updateJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrderController",
      "updateJSON",
      Seq(classOf[Int]),
      "PUT",
      this.prefix + """api/orders/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:51
  private[this] lazy val controllers_OrderController_deleteJSON39_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/orders/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_OrderController_deleteJSON39_invoker = createInvoker(
    OrderController_1.deleteJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrderController",
      "deleteJSON",
      Seq(classOf[Int]),
      "DELETE",
      this.prefix + """api/orders/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:52
  private[this] lazy val controllers_OrderController_create40_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders/create")))
  )
  private[this] lazy val controllers_OrderController_create40_invoker = createInvoker(
    OrderController_1.create,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrderController",
      "create",
      Nil,
      "GET",
      this.prefix + """orders/create""",
      """""",
      Seq()
    )
  )

  // @LINE:53
  private[this] lazy val controllers_OrderController_createHandle41_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders/createhandle")))
  )
  private[this] lazy val controllers_OrderController_createHandle41_invoker = createInvoker(
    OrderController_1.createHandle,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrderController",
      "createHandle",
      Nil,
      "POST",
      this.prefix + """orders/createhandle""",
      """""",
      Seq()
    )
  )

  // @LINE:54
  private[this] lazy val controllers_OrderController_list42_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders/all")))
  )
  private[this] lazy val controllers_OrderController_list42_invoker = createInvoker(
    OrderController_1.list,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrderController",
      "list",
      Nil,
      "GET",
      this.prefix + """orders/all""",
      """""",
      Seq()
    )
  )

  // @LINE:55
  private[this] lazy val controllers_OrderController_details43_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders/details/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_OrderController_details43_invoker = createInvoker(
    OrderController_1.details(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrderController",
      "details",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """orders/details/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:56
  private[this] lazy val controllers_OrderController_update44_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders/update/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_OrderController_update44_invoker = createInvoker(
    OrderController_1.update(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrderController",
      "update",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """orders/update/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:57
  private[this] lazy val controllers_OrderController_updateHandle45_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders/update")))
  )
  private[this] lazy val controllers_OrderController_updateHandle45_invoker = createInvoker(
    OrderController_1.updateHandle,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrderController",
      "updateHandle",
      Nil,
      "POST",
      this.prefix + """orders/update""",
      """""",
      Seq()
    )
  )

  // @LINE:58
  private[this] lazy val controllers_OrderController_delete46_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("orders/delete/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_OrderController_delete46_invoker = createInvoker(
    OrderController_1.delete(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrderController",
      "delete",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """orders/delete/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:60
  private[this] lazy val controllers_OrderProductsController_listJSON47_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/order_products")))
  )
  private[this] lazy val controllers_OrderProductsController_listJSON47_invoker = createInvoker(
    OrderProductsController_2.listJSON(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrderProductsController",
      "listJSON",
      Nil,
      "GET",
      this.prefix + """api/order_products""",
      """""",
      Seq()
    )
  )

  // @LINE:61
  private[this] lazy val controllers_OrderProductsController_detailsJSON48_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/order_products/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_OrderProductsController_detailsJSON48_invoker = createInvoker(
    OrderProductsController_2.detailsJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrderProductsController",
      "detailsJSON",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """api/order_products/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:62
  private[this] lazy val controllers_OrderProductsController_addJSON49_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/order_products")))
  )
  private[this] lazy val controllers_OrderProductsController_addJSON49_invoker = createInvoker(
    OrderProductsController_2.addJSON(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrderProductsController",
      "addJSON",
      Nil,
      "POST",
      this.prefix + """api/order_products""",
      """""",
      Seq()
    )
  )

  // @LINE:63
  private[this] lazy val controllers_OrderProductsController_updateJSON50_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/order_products/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_OrderProductsController_updateJSON50_invoker = createInvoker(
    OrderProductsController_2.updateJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrderProductsController",
      "updateJSON",
      Seq(classOf[Int]),
      "PUT",
      this.prefix + """api/order_products/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:64
  private[this] lazy val controllers_OrderProductsController_deleteJSON51_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/order_products/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_OrderProductsController_deleteJSON51_invoker = createInvoker(
    OrderProductsController_2.deleteJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrderProductsController",
      "deleteJSON",
      Seq(classOf[Int]),
      "DELETE",
      this.prefix + """api/order_products/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:65
  private[this] lazy val controllers_OrderProductsController_add52_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("order_products/add")))
  )
  private[this] lazy val controllers_OrderProductsController_add52_invoker = createInvoker(
    OrderProductsController_2.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrderProductsController",
      "add",
      Nil,
      "GET",
      this.prefix + """order_products/add""",
      """""",
      Seq()
    )
  )

  // @LINE:66
  private[this] lazy val controllers_OrderProductsController_addHandle53_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("order_products/addHandle")))
  )
  private[this] lazy val controllers_OrderProductsController_addHandle53_invoker = createInvoker(
    OrderProductsController_2.addHandle,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrderProductsController",
      "addHandle",
      Nil,
      "POST",
      this.prefix + """order_products/addHandle""",
      """""",
      Seq()
    )
  )

  // @LINE:67
  private[this] lazy val controllers_OrderProductsController_list54_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("order_products/all")))
  )
  private[this] lazy val controllers_OrderProductsController_list54_invoker = createInvoker(
    OrderProductsController_2.list,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrderProductsController",
      "list",
      Nil,
      "GET",
      this.prefix + """order_products/all""",
      """""",
      Seq()
    )
  )

  // @LINE:68
  private[this] lazy val controllers_OrderProductsController_details55_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("order_products/details/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_OrderProductsController_details55_invoker = createInvoker(
    OrderProductsController_2.details(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrderProductsController",
      "details",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """order_products/details/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:69
  private[this] lazy val controllers_OrderProductsController_update56_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("order_products/update/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_OrderProductsController_update56_invoker = createInvoker(
    OrderProductsController_2.update(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrderProductsController",
      "update",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """order_products/update/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:70
  private[this] lazy val controllers_OrderProductsController_updateHandle57_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("order_products/update")))
  )
  private[this] lazy val controllers_OrderProductsController_updateHandle57_invoker = createInvoker(
    OrderProductsController_2.updateHandle,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrderProductsController",
      "updateHandle",
      Nil,
      "POST",
      this.prefix + """order_products/update""",
      """""",
      Seq()
    )
  )

  // @LINE:71
  private[this] lazy val controllers_OrderProductsController_delete58_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("order_products/delete/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_OrderProductsController_delete58_invoker = createInvoker(
    OrderProductsController_2.delete(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.OrderProductsController",
      "delete",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """order_products/delete/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:73
  private[this] lazy val controllers_ProductController_listJSON59_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/products")))
  )
  private[this] lazy val controllers_ProductController_listJSON59_invoker = createInvoker(
    ProductController_9.listJSON(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductController",
      "listJSON",
      Nil,
      "GET",
      this.prefix + """api/products""",
      """""",
      Seq()
    )
  )

  // @LINE:74
  private[this] lazy val controllers_ProductController_extendedListJSON60_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/products/extended")))
  )
  private[this] lazy val controllers_ProductController_extendedListJSON60_invoker = createInvoker(
    ProductController_9.extendedListJSON(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductController",
      "extendedListJSON",
      Nil,
      "GET",
      this.prefix + """api/products/extended""",
      """""",
      Seq()
    )
  )

  // @LINE:75
  private[this] lazy val controllers_ProductController_detailsJSON61_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/products/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ProductController_detailsJSON61_invoker = createInvoker(
    ProductController_9.detailsJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductController",
      "detailsJSON",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """api/products/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:76
  private[this] lazy val controllers_ProductController_addJSON62_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/products")))
  )
  private[this] lazy val controllers_ProductController_addJSON62_invoker = createInvoker(
    ProductController_9.addJSON(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductController",
      "addJSON",
      Nil,
      "POST",
      this.prefix + """api/products""",
      """""",
      Seq()
    )
  )

  // @LINE:77
  private[this] lazy val controllers_ProductController_updateJSON63_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/products/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ProductController_updateJSON63_invoker = createInvoker(
    ProductController_9.updateJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductController",
      "updateJSON",
      Seq(classOf[Int]),
      "PUT",
      this.prefix + """api/products/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:78
  private[this] lazy val controllers_ProductController_deleteJSON64_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/products/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ProductController_deleteJSON64_invoker = createInvoker(
    ProductController_9.deleteJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductController",
      "deleteJSON",
      Seq(classOf[Int]),
      "DELETE",
      this.prefix + """api/products/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:79
  private[this] lazy val controllers_ProductController_create65_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/create")))
  )
  private[this] lazy val controllers_ProductController_create65_invoker = createInvoker(
    ProductController_9.create,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductController",
      "create",
      Nil,
      "GET",
      this.prefix + """products/create""",
      """""",
      Seq()
    )
  )

  // @LINE:80
  private[this] lazy val controllers_ProductController_createHandle66_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/createhandle")))
  )
  private[this] lazy val controllers_ProductController_createHandle66_invoker = createInvoker(
    ProductController_9.createHandle,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductController",
      "createHandle",
      Nil,
      "POST",
      this.prefix + """products/createhandle""",
      """""",
      Seq()
    )
  )

  // @LINE:81
  private[this] lazy val controllers_ProductController_list67_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/all")))
  )
  private[this] lazy val controllers_ProductController_list67_invoker = createInvoker(
    ProductController_9.list,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductController",
      "list",
      Nil,
      "GET",
      this.prefix + """products/all""",
      """""",
      Seq()
    )
  )

  // @LINE:82
  private[this] lazy val controllers_ProductController_extendedList68_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/all_extended")))
  )
  private[this] lazy val controllers_ProductController_extendedList68_invoker = createInvoker(
    ProductController_9.extendedList,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductController",
      "extendedList",
      Nil,
      "GET",
      this.prefix + """products/all_extended""",
      """""",
      Seq()
    )
  )

  // @LINE:83
  private[this] lazy val controllers_ProductController_details69_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/details/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ProductController_details69_invoker = createInvoker(
    ProductController_9.details(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductController",
      "details",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """products/details/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:84
  private[this] lazy val controllers_ProductController_update70_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/update/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ProductController_update70_invoker = createInvoker(
    ProductController_9.update(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductController",
      "update",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """products/update/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:85
  private[this] lazy val controllers_ProductController_updateHandle71_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/update")))
  )
  private[this] lazy val controllers_ProductController_updateHandle71_invoker = createInvoker(
    ProductController_9.updateHandle,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductController",
      "updateHandle",
      Nil,
      "POST",
      this.prefix + """products/update""",
      """""",
      Seq()
    )
  )

  // @LINE:86
  private[this] lazy val controllers_ProductController_delete72_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("products/delete/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ProductController_delete72_invoker = createInvoker(
    ProductController_9.delete(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductController",
      "delete",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """products/delete/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:88
  private[this] lazy val controllers_PromotionController_listJSON73_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/promotions")))
  )
  private[this] lazy val controllers_PromotionController_listJSON73_invoker = createInvoker(
    PromotionController_7.listJSON(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PromotionController",
      "listJSON",
      Nil,
      "GET",
      this.prefix + """api/promotions""",
      """""",
      Seq()
    )
  )

  // @LINE:89
  private[this] lazy val controllers_PromotionController_detailsJSON74_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/promotions/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_PromotionController_detailsJSON74_invoker = createInvoker(
    PromotionController_7.detailsJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PromotionController",
      "detailsJSON",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """api/promotions/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:90
  private[this] lazy val controllers_PromotionController_addJSON75_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/promotions")))
  )
  private[this] lazy val controllers_PromotionController_addJSON75_invoker = createInvoker(
    PromotionController_7.addJSON(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PromotionController",
      "addJSON",
      Nil,
      "POST",
      this.prefix + """api/promotions""",
      """""",
      Seq()
    )
  )

  // @LINE:91
  private[this] lazy val controllers_PromotionController_updateJSON76_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/promotions/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_PromotionController_updateJSON76_invoker = createInvoker(
    PromotionController_7.updateJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PromotionController",
      "updateJSON",
      Seq(classOf[Int]),
      "PUT",
      this.prefix + """api/promotions/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:92
  private[this] lazy val controllers_PromotionController_deleteJSON77_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/promotions/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_PromotionController_deleteJSON77_invoker = createInvoker(
    PromotionController_7.deleteJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PromotionController",
      "deleteJSON",
      Seq(classOf[Int]),
      "DELETE",
      this.prefix + """api/promotions/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:93
  private[this] lazy val controllers_PromotionController_create78_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("promotions/create")))
  )
  private[this] lazy val controllers_PromotionController_create78_invoker = createInvoker(
    PromotionController_7.create,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PromotionController",
      "create",
      Nil,
      "GET",
      this.prefix + """promotions/create""",
      """""",
      Seq()
    )
  )

  // @LINE:94
  private[this] lazy val controllers_PromotionController_createHandle79_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("promotions/createhandle")))
  )
  private[this] lazy val controllers_PromotionController_createHandle79_invoker = createInvoker(
    PromotionController_7.createHandle,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PromotionController",
      "createHandle",
      Nil,
      "POST",
      this.prefix + """promotions/createhandle""",
      """""",
      Seq()
    )
  )

  // @LINE:95
  private[this] lazy val controllers_PromotionController_list80_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("promotions/all")))
  )
  private[this] lazy val controllers_PromotionController_list80_invoker = createInvoker(
    PromotionController_7.list,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PromotionController",
      "list",
      Nil,
      "GET",
      this.prefix + """promotions/all""",
      """""",
      Seq()
    )
  )

  // @LINE:96
  private[this] lazy val controllers_PromotionController_details81_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("promotions/details/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_PromotionController_details81_invoker = createInvoker(
    PromotionController_7.details(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PromotionController",
      "details",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """promotions/details/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:97
  private[this] lazy val controllers_PromotionController_update82_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("promotions/update/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_PromotionController_update82_invoker = createInvoker(
    PromotionController_7.update(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PromotionController",
      "update",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """promotions/update/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:98
  private[this] lazy val controllers_PromotionController_updateHandle83_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("promotions/update")))
  )
  private[this] lazy val controllers_PromotionController_updateHandle83_invoker = createInvoker(
    PromotionController_7.updateHandle,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.PromotionController",
      "updateHandle",
      Nil,
      "POST",
      this.prefix + """promotions/update""",
      """""",
      Seq()
    )
  )

  // @LINE:99
  private[this] lazy val controllers_ProductController_delete84_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("promotions/delete/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ProductController_delete84_invoker = createInvoker(
    ProductController_9.delete(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ProductController",
      "delete",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """promotions/delete/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:101
  private[this] lazy val controllers_ReviewController_listJSON85_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/reviews")))
  )
  private[this] lazy val controllers_ReviewController_listJSON85_invoker = createInvoker(
    ReviewController_3.listJSON(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ReviewController",
      "listJSON",
      Nil,
      "GET",
      this.prefix + """api/reviews""",
      """""",
      Seq()
    )
  )

  // @LINE:102
  private[this] lazy val controllers_ReviewController_listProductJSON86_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/product_reviews/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ReviewController_listProductJSON86_invoker = createInvoker(
    ReviewController_3.listProductJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ReviewController",
      "listProductJSON",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """api/product_reviews/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:103
  private[this] lazy val controllers_ReviewController_detailsJSON87_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/reviews/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ReviewController_detailsJSON87_invoker = createInvoker(
    ReviewController_3.detailsJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ReviewController",
      "detailsJSON",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """api/reviews/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:104
  private[this] lazy val controllers_ReviewController_createJSON88_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/reviews")))
  )
  private[this] lazy val controllers_ReviewController_createJSON88_invoker = createInvoker(
    ReviewController_3.createJSON(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ReviewController",
      "createJSON",
      Nil,
      "POST",
      this.prefix + """api/reviews""",
      """""",
      Seq()
    )
  )

  // @LINE:105
  private[this] lazy val controllers_ReviewController_updateJSON89_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/reviews/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ReviewController_updateJSON89_invoker = createInvoker(
    ReviewController_3.updateJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ReviewController",
      "updateJSON",
      Seq(classOf[Int]),
      "PUT",
      this.prefix + """api/reviews/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:106
  private[this] lazy val controllers_ReviewController_deleteJSON90_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/reviews/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ReviewController_deleteJSON90_invoker = createInvoker(
    ReviewController_3.deleteJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ReviewController",
      "deleteJSON",
      Seq(classOf[Int]),
      "DELETE",
      this.prefix + """api/reviews/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:107
  private[this] lazy val controllers_ReviewController_create91_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("reviews/create")))
  )
  private[this] lazy val controllers_ReviewController_create91_invoker = createInvoker(
    ReviewController_3.create,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ReviewController",
      "create",
      Nil,
      "GET",
      this.prefix + """reviews/create""",
      """""",
      Seq()
    )
  )

  // @LINE:108
  private[this] lazy val controllers_ReviewController_createHandle92_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("reviews/createhandle")))
  )
  private[this] lazy val controllers_ReviewController_createHandle92_invoker = createInvoker(
    ReviewController_3.createHandle,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ReviewController",
      "createHandle",
      Nil,
      "POST",
      this.prefix + """reviews/createhandle""",
      """""",
      Seq()
    )
  )

  // @LINE:109
  private[this] lazy val controllers_ReviewController_list93_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("reviews/all")))
  )
  private[this] lazy val controllers_ReviewController_list93_invoker = createInvoker(
    ReviewController_3.list,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ReviewController",
      "list",
      Nil,
      "GET",
      this.prefix + """reviews/all""",
      """""",
      Seq()
    )
  )

  // @LINE:110
  private[this] lazy val controllers_ReviewController_details94_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("reviews/details/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ReviewController_details94_invoker = createInvoker(
    ReviewController_3.details(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ReviewController",
      "details",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """reviews/details/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:111
  private[this] lazy val controllers_ReviewController_update95_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("reviews/update/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ReviewController_update95_invoker = createInvoker(
    ReviewController_3.update(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ReviewController",
      "update",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """reviews/update/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:112
  private[this] lazy val controllers_ReviewController_updateHandle96_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("reviews/update")))
  )
  private[this] lazy val controllers_ReviewController_updateHandle96_invoker = createInvoker(
    ReviewController_3.updateHandle,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ReviewController",
      "updateHandle",
      Nil,
      "POST",
      this.prefix + """reviews/update""",
      """""",
      Seq()
    )
  )

  // @LINE:113
  private[this] lazy val controllers_ReviewController_delete97_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("reviews/delete/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_ReviewController_delete97_invoker = createInvoker(
    ReviewController_3.delete(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.ReviewController",
      "delete",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """reviews/delete/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:115
  private[this] lazy val controllers_StockController_listJSON98_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/stocks")))
  )
  private[this] lazy val controllers_StockController_listJSON98_invoker = createInvoker(
    StockController_8.listJSON(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.StockController",
      "listJSON",
      Nil,
      "GET",
      this.prefix + """api/stocks""",
      """""",
      Seq()
    )
  )

  // @LINE:116
  private[this] lazy val controllers_StockController_detailsJSON99_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/stocks/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_StockController_detailsJSON99_invoker = createInvoker(
    StockController_8.detailsJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.StockController",
      "detailsJSON",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """api/stocks/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:117
  private[this] lazy val controllers_StockController_addJSON100_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/stocks")))
  )
  private[this] lazy val controllers_StockController_addJSON100_invoker = createInvoker(
    StockController_8.addJSON(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.StockController",
      "addJSON",
      Nil,
      "POST",
      this.prefix + """api/stocks""",
      """""",
      Seq()
    )
  )

  // @LINE:118
  private[this] lazy val controllers_StockController_updateJSON101_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/stocks/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_StockController_updateJSON101_invoker = createInvoker(
    StockController_8.updateJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.StockController",
      "updateJSON",
      Seq(classOf[Int]),
      "PUT",
      this.prefix + """api/stocks/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:119
  private[this] lazy val controllers_StockController_deleteJSON102_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/stocks/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_StockController_deleteJSON102_invoker = createInvoker(
    StockController_8.deleteJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.StockController",
      "deleteJSON",
      Seq(classOf[Int]),
      "DELETE",
      this.prefix + """api/stocks/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:120
  private[this] lazy val controllers_StockController_add103_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("stocks/add")))
  )
  private[this] lazy val controllers_StockController_add103_invoker = createInvoker(
    StockController_8.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.StockController",
      "add",
      Nil,
      "GET",
      this.prefix + """stocks/add""",
      """""",
      Seq()
    )
  )

  // @LINE:121
  private[this] lazy val controllers_StockController_addHandle104_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("stocks/addhandle")))
  )
  private[this] lazy val controllers_StockController_addHandle104_invoker = createInvoker(
    StockController_8.addHandle,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.StockController",
      "addHandle",
      Nil,
      "POST",
      this.prefix + """stocks/addhandle""",
      """""",
      Seq()
    )
  )

  // @LINE:122
  private[this] lazy val controllers_StockController_list105_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("stocks/all")))
  )
  private[this] lazy val controllers_StockController_list105_invoker = createInvoker(
    StockController_8.list,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.StockController",
      "list",
      Nil,
      "GET",
      this.prefix + """stocks/all""",
      """""",
      Seq()
    )
  )

  // @LINE:123
  private[this] lazy val controllers_StockController_details106_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("stocks/details/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_StockController_details106_invoker = createInvoker(
    StockController_8.details(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.StockController",
      "details",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """stocks/details/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:124
  private[this] lazy val controllers_StockController_update107_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("stocks/update/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_StockController_update107_invoker = createInvoker(
    StockController_8.update(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.StockController",
      "update",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """stocks/update/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:125
  private[this] lazy val controllers_StockController_updateHandle108_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("stocks/update")))
  )
  private[this] lazy val controllers_StockController_updateHandle108_invoker = createInvoker(
    StockController_8.updateHandle,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.StockController",
      "updateHandle",
      Nil,
      "POST",
      this.prefix + """stocks/update""",
      """""",
      Seq()
    )
  )

  // @LINE:126
  private[this] lazy val controllers_StockController_delete109_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("stocks/delete/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_StockController_delete109_invoker = createInvoker(
    StockController_8.delete(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.StockController",
      "delete",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """stocks/delete/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:128
  private[this] lazy val controllers_WishlistController_listJSON110_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/wishlists")))
  )
  private[this] lazy val controllers_WishlistController_listJSON110_invoker = createInvoker(
    WishlistController_12.listJSON(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WishlistController",
      "listJSON",
      Nil,
      "GET",
      this.prefix + """api/wishlists""",
      """""",
      Seq()
    )
  )

  // @LINE:129
  private[this] lazy val controllers_WishlistController_detailsJSON111_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/wishlists/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_WishlistController_detailsJSON111_invoker = createInvoker(
    WishlistController_12.detailsJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WishlistController",
      "detailsJSON",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """api/wishlists/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:130
  private[this] lazy val controllers_WishlistController_addJSON112_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/wishlists")))
  )
  private[this] lazy val controllers_WishlistController_addJSON112_invoker = createInvoker(
    WishlistController_12.addJSON(),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WishlistController",
      "addJSON",
      Nil,
      "POST",
      this.prefix + """api/wishlists""",
      """""",
      Seq()
    )
  )

  // @LINE:131
  private[this] lazy val controllers_WishlistController_updateJSON113_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/wishlists/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_WishlistController_updateJSON113_invoker = createInvoker(
    WishlistController_12.updateJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WishlistController",
      "updateJSON",
      Seq(classOf[Int]),
      "PUT",
      this.prefix + """api/wishlists/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:132
  private[this] lazy val controllers_WishlistController_deleteJSON114_route = Route("DELETE",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/wishlists/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_WishlistController_deleteJSON114_invoker = createInvoker(
    WishlistController_12.deleteJSON(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WishlistController",
      "deleteJSON",
      Seq(classOf[Int]),
      "DELETE",
      this.prefix + """api/wishlists/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:133
  private[this] lazy val controllers_WishlistController_add115_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("wishlists/add")))
  )
  private[this] lazy val controllers_WishlistController_add115_invoker = createInvoker(
    WishlistController_12.add,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WishlistController",
      "add",
      Nil,
      "GET",
      this.prefix + """wishlists/add""",
      """""",
      Seq()
    )
  )

  // @LINE:134
  private[this] lazy val controllers_WishlistController_addHandle116_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("wishlists/addhandle")))
  )
  private[this] lazy val controllers_WishlistController_addHandle116_invoker = createInvoker(
    WishlistController_12.addHandle,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WishlistController",
      "addHandle",
      Nil,
      "POST",
      this.prefix + """wishlists/addhandle""",
      """""",
      Seq()
    )
  )

  // @LINE:135
  private[this] lazy val controllers_WishlistController_list117_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("wishlists/all")))
  )
  private[this] lazy val controllers_WishlistController_list117_invoker = createInvoker(
    WishlistController_12.list,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WishlistController",
      "list",
      Nil,
      "GET",
      this.prefix + """wishlists/all""",
      """""",
      Seq()
    )
  )

  // @LINE:136
  private[this] lazy val controllers_WishlistController_details118_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("wishlists/details/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_WishlistController_details118_invoker = createInvoker(
    WishlistController_12.details(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WishlistController",
      "details",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """wishlists/details/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:137
  private[this] lazy val controllers_WishlistController_update119_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("wishlists/update/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_WishlistController_update119_invoker = createInvoker(
    WishlistController_12.update(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WishlistController",
      "update",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """wishlists/update/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:138
  private[this] lazy val controllers_WishlistController_updateHandle120_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("wishlists/update")))
  )
  private[this] lazy val controllers_WishlistController_updateHandle120_invoker = createInvoker(
    WishlistController_12.updateHandle,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WishlistController",
      "updateHandle",
      Nil,
      "POST",
      this.prefix + """wishlists/update""",
      """""",
      Seq()
    )
  )

  // @LINE:139
  private[this] lazy val controllers_WishlistController_delete121_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("wishlists/delete/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_WishlistController_delete121_invoker = createInvoker(
    WishlistController_12.delete(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WishlistController",
      "delete",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """wishlists/delete/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:141
  private[this] lazy val controllers_RegisterController_submit122_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("auth/register")))
  )
  private[this] lazy val controllers_RegisterController_submit122_invoker = createInvoker(
    RegisterController_13.submit,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.RegisterController",
      "submit",
      Nil,
      "POST",
      this.prefix + """auth/register""",
      """""",
      Seq()
    )
  )

  // @LINE:142
  private[this] lazy val controllers_LoginController_submit123_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("auth/login")))
  )
  private[this] lazy val controllers_LoginController_submit123_invoker = createInvoker(
    LoginController_4.submit,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.LoginController",
      "submit",
      Nil,
      "POST",
      this.prefix + """auth/login""",
      """""",
      Seq()
    )
  )

  // @LINE:143
  private[this] lazy val controllers_LoginController_signOut124_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("auth/logout")))
  )
  private[this] lazy val controllers_LoginController_signOut124_invoker = createInvoker(
    LoginController_4.signOut,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.LoginController",
      "signOut",
      Nil,
      "POST",
      this.prefix + """auth/logout""",
      """""",
      Seq()
    )
  )

  // @LINE:144
  private[this] lazy val controllers_SocialProviderController_authenticate125_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("auth/provider/"), DynamicPart("provider", """[^/]+""",true)))
  )
  private[this] lazy val controllers_SocialProviderController_authenticate125_invoker = createInvoker(
    SocialProviderController_10.authenticate(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.SocialProviderController",
      "authenticate",
      Seq(classOf[String]),
      "GET",
      this.prefix + """auth/provider/""" + "$" + """provider<[^/]+>""",
      """""",
      Seq()
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:5
    case controllers_HomeController_index0_route(params@_) =>
      call { 
        controllers_HomeController_index0_invoker.call(HomeController_14.index)
      }
  
    // @LINE:8
    case controllers_Assets_versioned1_route(params@_) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned1_invoker.call(Assets_11.versioned(path, file))
      }
  
    // @LINE:11
    case controllers_CartController_detailsUserJSON2_route(params@_) =>
      call { 
        controllers_CartController_detailsUserJSON2_invoker.call(CartController_0.detailsUserJSON())
      }
  
    // @LINE:12
    case controllers_CartController_deleteUser3_route(params@_) =>
      call { 
        controllers_CartController_deleteUser3_invoker.call(CartController_0.deleteUser())
      }
  
    // @LINE:13
    case controllers_CartController_detailsUserExtendedJSON4_route(params@_) =>
      call { 
        controllers_CartController_detailsUserExtendedJSON4_invoker.call(CartController_0.detailsUserExtendedJSON())
      }
  
    // @LINE:14
    case controllers_CartController_addJSON5_route(params@_) =>
      call { 
        controllers_CartController_addJSON5_invoker.call(CartController_0.addJSON())
      }
  
    // @LINE:15
    case controllers_CartController_updateJSON6_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_CartController_updateJSON6_invoker.call(CartController_0.updateJSON(id))
      }
  
    // @LINE:16
    case controllers_CartController_deleteJSON7_route(params@_) =>
      call(params.fromPath[String]("uid", None)) { (uid) =>
        controllers_CartController_deleteJSON7_invoker.call(CartController_0.deleteJSON(uid))
      }
  
    // @LINE:17
    case controllers_CartController_add8_route(params@_) =>
      call { 
        controllers_CartController_add8_invoker.call(CartController_0.add)
      }
  
    // @LINE:18
    case controllers_CartController_addHandle9_route(params@_) =>
      call { 
        controllers_CartController_addHandle9_invoker.call(CartController_0.addHandle)
      }
  
    // @LINE:19
    case controllers_CartController_list10_route(params@_) =>
      call { 
        controllers_CartController_list10_invoker.call(CartController_0.list)
      }
  
    // @LINE:21
    case controllers_CategoryController_listJSON11_route(params@_) =>
      call { 
        controllers_CategoryController_listJSON11_invoker.call(CategoryController_6.listJSON())
      }
  
    // @LINE:22
    case controllers_CategoryController_detailsJSON12_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_CategoryController_detailsJSON12_invoker.call(CategoryController_6.detailsJSON(id))
      }
  
    // @LINE:23
    case controllers_CategoryController_createJSON13_route(params@_) =>
      call { 
        controllers_CategoryController_createJSON13_invoker.call(CategoryController_6.createJSON())
      }
  
    // @LINE:24
    case controllers_CategoryController_updateJSON14_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_CategoryController_updateJSON14_invoker.call(CategoryController_6.updateJSON(id))
      }
  
    // @LINE:25
    case controllers_CategoryController_deleteJSON15_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_CategoryController_deleteJSON15_invoker.call(CategoryController_6.deleteJSON(id))
      }
  
    // @LINE:26
    case controllers_CategoryController_create16_route(params@_) =>
      call { 
        controllers_CategoryController_create16_invoker.call(CategoryController_6.create)
      }
  
    // @LINE:27
    case controllers_CategoryController_createHandle17_route(params@_) =>
      call { 
        controllers_CategoryController_createHandle17_invoker.call(CategoryController_6.createHandle)
      }
  
    // @LINE:28
    case controllers_CategoryController_list18_route(params@_) =>
      call { 
        controllers_CategoryController_list18_invoker.call(CategoryController_6.list)
      }
  
    // @LINE:29
    case controllers_CategoryController_details19_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_CategoryController_details19_invoker.call(CategoryController_6.details(id))
      }
  
    // @LINE:30
    case controllers_CategoryController_update20_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_CategoryController_update20_invoker.call(CategoryController_6.update(id))
      }
  
    // @LINE:31
    case controllers_CategoryController_updateHandle21_route(params@_) =>
      call { 
        controllers_CategoryController_updateHandle21_invoker.call(CategoryController_6.updateHandle)
      }
  
    // @LINE:32
    case controllers_CategoryController_delete22_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_CategoryController_delete22_invoker.call(CategoryController_6.delete(id))
      }
  
    // @LINE:34
    case controllers_InvoiceController_listJSON23_route(params@_) =>
      call { 
        controllers_InvoiceController_listJSON23_invoker.call(InvoiceController_5.listJSON())
      }
  
    // @LINE:35
    case controllers_InvoiceController_detailsJSON24_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_InvoiceController_detailsJSON24_invoker.call(InvoiceController_5.detailsJSON(id))
      }
  
    // @LINE:36
    case controllers_InvoiceController_createJSON25_route(params@_) =>
      call { 
        controllers_InvoiceController_createJSON25_invoker.call(InvoiceController_5.createJSON())
      }
  
    // @LINE:37
    case controllers_InvoiceController_updateJSON26_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_InvoiceController_updateJSON26_invoker.call(InvoiceController_5.updateJSON(id))
      }
  
    // @LINE:38
    case controllers_InvoiceController_deleteJSON27_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_InvoiceController_deleteJSON27_invoker.call(InvoiceController_5.deleteJSON(id))
      }
  
    // @LINE:39
    case controllers_InvoiceController_create28_route(params@_) =>
      call { 
        controllers_InvoiceController_create28_invoker.call(InvoiceController_5.create)
      }
  
    // @LINE:40
    case controllers_InvoiceController_createHandle29_route(params@_) =>
      call { 
        controllers_InvoiceController_createHandle29_invoker.call(InvoiceController_5.createHandle)
      }
  
    // @LINE:41
    case controllers_InvoiceController_list30_route(params@_) =>
      call { 
        controllers_InvoiceController_list30_invoker.call(InvoiceController_5.list)
      }
  
    // @LINE:42
    case controllers_InvoiceController_details31_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_InvoiceController_details31_invoker.call(InvoiceController_5.details(id))
      }
  
    // @LINE:43
    case controllers_InvoiceController_update32_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_InvoiceController_update32_invoker.call(InvoiceController_5.update(id))
      }
  
    // @LINE:44
    case controllers_InvoiceController_updateHandle33_route(params@_) =>
      call { 
        controllers_InvoiceController_updateHandle33_invoker.call(InvoiceController_5.updateHandle)
      }
  
    // @LINE:45
    case controllers_InvoiceController_delete34_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_InvoiceController_delete34_invoker.call(InvoiceController_5.delete(id))
      }
  
    // @LINE:47
    case controllers_OrderController_listJSON35_route(params@_) =>
      call { 
        controllers_OrderController_listJSON35_invoker.call(OrderController_1.listJSON())
      }
  
    // @LINE:48
    case controllers_OrderController_detailsJSON36_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_OrderController_detailsJSON36_invoker.call(OrderController_1.detailsJSON(id))
      }
  
    // @LINE:49
    case controllers_OrderController_createJSON37_route(params@_) =>
      call { 
        controllers_OrderController_createJSON37_invoker.call(OrderController_1.createJSON())
      }
  
    // @LINE:50
    case controllers_OrderController_updateJSON38_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_OrderController_updateJSON38_invoker.call(OrderController_1.updateJSON(id))
      }
  
    // @LINE:51
    case controllers_OrderController_deleteJSON39_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_OrderController_deleteJSON39_invoker.call(OrderController_1.deleteJSON(id))
      }
  
    // @LINE:52
    case controllers_OrderController_create40_route(params@_) =>
      call { 
        controllers_OrderController_create40_invoker.call(OrderController_1.create)
      }
  
    // @LINE:53
    case controllers_OrderController_createHandle41_route(params@_) =>
      call { 
        controllers_OrderController_createHandle41_invoker.call(OrderController_1.createHandle)
      }
  
    // @LINE:54
    case controllers_OrderController_list42_route(params@_) =>
      call { 
        controllers_OrderController_list42_invoker.call(OrderController_1.list)
      }
  
    // @LINE:55
    case controllers_OrderController_details43_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_OrderController_details43_invoker.call(OrderController_1.details(id))
      }
  
    // @LINE:56
    case controllers_OrderController_update44_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_OrderController_update44_invoker.call(OrderController_1.update(id))
      }
  
    // @LINE:57
    case controllers_OrderController_updateHandle45_route(params@_) =>
      call { 
        controllers_OrderController_updateHandle45_invoker.call(OrderController_1.updateHandle)
      }
  
    // @LINE:58
    case controllers_OrderController_delete46_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_OrderController_delete46_invoker.call(OrderController_1.delete(id))
      }
  
    // @LINE:60
    case controllers_OrderProductsController_listJSON47_route(params@_) =>
      call { 
        controllers_OrderProductsController_listJSON47_invoker.call(OrderProductsController_2.listJSON())
      }
  
    // @LINE:61
    case controllers_OrderProductsController_detailsJSON48_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_OrderProductsController_detailsJSON48_invoker.call(OrderProductsController_2.detailsJSON(id))
      }
  
    // @LINE:62
    case controllers_OrderProductsController_addJSON49_route(params@_) =>
      call { 
        controllers_OrderProductsController_addJSON49_invoker.call(OrderProductsController_2.addJSON())
      }
  
    // @LINE:63
    case controllers_OrderProductsController_updateJSON50_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_OrderProductsController_updateJSON50_invoker.call(OrderProductsController_2.updateJSON(id))
      }
  
    // @LINE:64
    case controllers_OrderProductsController_deleteJSON51_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_OrderProductsController_deleteJSON51_invoker.call(OrderProductsController_2.deleteJSON(id))
      }
  
    // @LINE:65
    case controllers_OrderProductsController_add52_route(params@_) =>
      call { 
        controllers_OrderProductsController_add52_invoker.call(OrderProductsController_2.add)
      }
  
    // @LINE:66
    case controllers_OrderProductsController_addHandle53_route(params@_) =>
      call { 
        controllers_OrderProductsController_addHandle53_invoker.call(OrderProductsController_2.addHandle)
      }
  
    // @LINE:67
    case controllers_OrderProductsController_list54_route(params@_) =>
      call { 
        controllers_OrderProductsController_list54_invoker.call(OrderProductsController_2.list)
      }
  
    // @LINE:68
    case controllers_OrderProductsController_details55_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_OrderProductsController_details55_invoker.call(OrderProductsController_2.details(id))
      }
  
    // @LINE:69
    case controllers_OrderProductsController_update56_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_OrderProductsController_update56_invoker.call(OrderProductsController_2.update(id))
      }
  
    // @LINE:70
    case controllers_OrderProductsController_updateHandle57_route(params@_) =>
      call { 
        controllers_OrderProductsController_updateHandle57_invoker.call(OrderProductsController_2.updateHandle)
      }
  
    // @LINE:71
    case controllers_OrderProductsController_delete58_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_OrderProductsController_delete58_invoker.call(OrderProductsController_2.delete(id))
      }
  
    // @LINE:73
    case controllers_ProductController_listJSON59_route(params@_) =>
      call { 
        controllers_ProductController_listJSON59_invoker.call(ProductController_9.listJSON())
      }
  
    // @LINE:74
    case controllers_ProductController_extendedListJSON60_route(params@_) =>
      call { 
        controllers_ProductController_extendedListJSON60_invoker.call(ProductController_9.extendedListJSON())
      }
  
    // @LINE:75
    case controllers_ProductController_detailsJSON61_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_ProductController_detailsJSON61_invoker.call(ProductController_9.detailsJSON(id))
      }
  
    // @LINE:76
    case controllers_ProductController_addJSON62_route(params@_) =>
      call { 
        controllers_ProductController_addJSON62_invoker.call(ProductController_9.addJSON())
      }
  
    // @LINE:77
    case controllers_ProductController_updateJSON63_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_ProductController_updateJSON63_invoker.call(ProductController_9.updateJSON(id))
      }
  
    // @LINE:78
    case controllers_ProductController_deleteJSON64_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_ProductController_deleteJSON64_invoker.call(ProductController_9.deleteJSON(id))
      }
  
    // @LINE:79
    case controllers_ProductController_create65_route(params@_) =>
      call { 
        controllers_ProductController_create65_invoker.call(ProductController_9.create)
      }
  
    // @LINE:80
    case controllers_ProductController_createHandle66_route(params@_) =>
      call { 
        controllers_ProductController_createHandle66_invoker.call(ProductController_9.createHandle)
      }
  
    // @LINE:81
    case controllers_ProductController_list67_route(params@_) =>
      call { 
        controllers_ProductController_list67_invoker.call(ProductController_9.list)
      }
  
    // @LINE:82
    case controllers_ProductController_extendedList68_route(params@_) =>
      call { 
        controllers_ProductController_extendedList68_invoker.call(ProductController_9.extendedList)
      }
  
    // @LINE:83
    case controllers_ProductController_details69_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_ProductController_details69_invoker.call(ProductController_9.details(id))
      }
  
    // @LINE:84
    case controllers_ProductController_update70_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_ProductController_update70_invoker.call(ProductController_9.update(id))
      }
  
    // @LINE:85
    case controllers_ProductController_updateHandle71_route(params@_) =>
      call { 
        controllers_ProductController_updateHandle71_invoker.call(ProductController_9.updateHandle)
      }
  
    // @LINE:86
    case controllers_ProductController_delete72_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_ProductController_delete72_invoker.call(ProductController_9.delete(id))
      }
  
    // @LINE:88
    case controllers_PromotionController_listJSON73_route(params@_) =>
      call { 
        controllers_PromotionController_listJSON73_invoker.call(PromotionController_7.listJSON())
      }
  
    // @LINE:89
    case controllers_PromotionController_detailsJSON74_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_PromotionController_detailsJSON74_invoker.call(PromotionController_7.detailsJSON(id))
      }
  
    // @LINE:90
    case controllers_PromotionController_addJSON75_route(params@_) =>
      call { 
        controllers_PromotionController_addJSON75_invoker.call(PromotionController_7.addJSON())
      }
  
    // @LINE:91
    case controllers_PromotionController_updateJSON76_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_PromotionController_updateJSON76_invoker.call(PromotionController_7.updateJSON(id))
      }
  
    // @LINE:92
    case controllers_PromotionController_deleteJSON77_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_PromotionController_deleteJSON77_invoker.call(PromotionController_7.deleteJSON(id))
      }
  
    // @LINE:93
    case controllers_PromotionController_create78_route(params@_) =>
      call { 
        controllers_PromotionController_create78_invoker.call(PromotionController_7.create)
      }
  
    // @LINE:94
    case controllers_PromotionController_createHandle79_route(params@_) =>
      call { 
        controllers_PromotionController_createHandle79_invoker.call(PromotionController_7.createHandle)
      }
  
    // @LINE:95
    case controllers_PromotionController_list80_route(params@_) =>
      call { 
        controllers_PromotionController_list80_invoker.call(PromotionController_7.list)
      }
  
    // @LINE:96
    case controllers_PromotionController_details81_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_PromotionController_details81_invoker.call(PromotionController_7.details(id))
      }
  
    // @LINE:97
    case controllers_PromotionController_update82_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_PromotionController_update82_invoker.call(PromotionController_7.update(id))
      }
  
    // @LINE:98
    case controllers_PromotionController_updateHandle83_route(params@_) =>
      call { 
        controllers_PromotionController_updateHandle83_invoker.call(PromotionController_7.updateHandle)
      }
  
    // @LINE:99
    case controllers_ProductController_delete84_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_ProductController_delete84_invoker.call(ProductController_9.delete(id))
      }
  
    // @LINE:101
    case controllers_ReviewController_listJSON85_route(params@_) =>
      call { 
        controllers_ReviewController_listJSON85_invoker.call(ReviewController_3.listJSON())
      }
  
    // @LINE:102
    case controllers_ReviewController_listProductJSON86_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_ReviewController_listProductJSON86_invoker.call(ReviewController_3.listProductJSON(id))
      }
  
    // @LINE:103
    case controllers_ReviewController_detailsJSON87_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_ReviewController_detailsJSON87_invoker.call(ReviewController_3.detailsJSON(id))
      }
  
    // @LINE:104
    case controllers_ReviewController_createJSON88_route(params@_) =>
      call { 
        controllers_ReviewController_createJSON88_invoker.call(ReviewController_3.createJSON())
      }
  
    // @LINE:105
    case controllers_ReviewController_updateJSON89_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_ReviewController_updateJSON89_invoker.call(ReviewController_3.updateJSON(id))
      }
  
    // @LINE:106
    case controllers_ReviewController_deleteJSON90_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_ReviewController_deleteJSON90_invoker.call(ReviewController_3.deleteJSON(id))
      }
  
    // @LINE:107
    case controllers_ReviewController_create91_route(params@_) =>
      call { 
        controllers_ReviewController_create91_invoker.call(ReviewController_3.create)
      }
  
    // @LINE:108
    case controllers_ReviewController_createHandle92_route(params@_) =>
      call { 
        controllers_ReviewController_createHandle92_invoker.call(ReviewController_3.createHandle)
      }
  
    // @LINE:109
    case controllers_ReviewController_list93_route(params@_) =>
      call { 
        controllers_ReviewController_list93_invoker.call(ReviewController_3.list)
      }
  
    // @LINE:110
    case controllers_ReviewController_details94_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_ReviewController_details94_invoker.call(ReviewController_3.details(id))
      }
  
    // @LINE:111
    case controllers_ReviewController_update95_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_ReviewController_update95_invoker.call(ReviewController_3.update(id))
      }
  
    // @LINE:112
    case controllers_ReviewController_updateHandle96_route(params@_) =>
      call { 
        controllers_ReviewController_updateHandle96_invoker.call(ReviewController_3.updateHandle)
      }
  
    // @LINE:113
    case controllers_ReviewController_delete97_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_ReviewController_delete97_invoker.call(ReviewController_3.delete(id))
      }
  
    // @LINE:115
    case controllers_StockController_listJSON98_route(params@_) =>
      call { 
        controllers_StockController_listJSON98_invoker.call(StockController_8.listJSON())
      }
  
    // @LINE:116
    case controllers_StockController_detailsJSON99_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_StockController_detailsJSON99_invoker.call(StockController_8.detailsJSON(id))
      }
  
    // @LINE:117
    case controllers_StockController_addJSON100_route(params@_) =>
      call { 
        controllers_StockController_addJSON100_invoker.call(StockController_8.addJSON())
      }
  
    // @LINE:118
    case controllers_StockController_updateJSON101_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_StockController_updateJSON101_invoker.call(StockController_8.updateJSON(id))
      }
  
    // @LINE:119
    case controllers_StockController_deleteJSON102_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_StockController_deleteJSON102_invoker.call(StockController_8.deleteJSON(id))
      }
  
    // @LINE:120
    case controllers_StockController_add103_route(params@_) =>
      call { 
        controllers_StockController_add103_invoker.call(StockController_8.add)
      }
  
    // @LINE:121
    case controllers_StockController_addHandle104_route(params@_) =>
      call { 
        controllers_StockController_addHandle104_invoker.call(StockController_8.addHandle)
      }
  
    // @LINE:122
    case controllers_StockController_list105_route(params@_) =>
      call { 
        controllers_StockController_list105_invoker.call(StockController_8.list)
      }
  
    // @LINE:123
    case controllers_StockController_details106_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_StockController_details106_invoker.call(StockController_8.details(id))
      }
  
    // @LINE:124
    case controllers_StockController_update107_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_StockController_update107_invoker.call(StockController_8.update(id))
      }
  
    // @LINE:125
    case controllers_StockController_updateHandle108_route(params@_) =>
      call { 
        controllers_StockController_updateHandle108_invoker.call(StockController_8.updateHandle)
      }
  
    // @LINE:126
    case controllers_StockController_delete109_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_StockController_delete109_invoker.call(StockController_8.delete(id))
      }
  
    // @LINE:128
    case controllers_WishlistController_listJSON110_route(params@_) =>
      call { 
        controllers_WishlistController_listJSON110_invoker.call(WishlistController_12.listJSON())
      }
  
    // @LINE:129
    case controllers_WishlistController_detailsJSON111_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_WishlistController_detailsJSON111_invoker.call(WishlistController_12.detailsJSON(id))
      }
  
    // @LINE:130
    case controllers_WishlistController_addJSON112_route(params@_) =>
      call { 
        controllers_WishlistController_addJSON112_invoker.call(WishlistController_12.addJSON())
      }
  
    // @LINE:131
    case controllers_WishlistController_updateJSON113_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_WishlistController_updateJSON113_invoker.call(WishlistController_12.updateJSON(id))
      }
  
    // @LINE:132
    case controllers_WishlistController_deleteJSON114_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_WishlistController_deleteJSON114_invoker.call(WishlistController_12.deleteJSON(id))
      }
  
    // @LINE:133
    case controllers_WishlistController_add115_route(params@_) =>
      call { 
        controllers_WishlistController_add115_invoker.call(WishlistController_12.add)
      }
  
    // @LINE:134
    case controllers_WishlistController_addHandle116_route(params@_) =>
      call { 
        controllers_WishlistController_addHandle116_invoker.call(WishlistController_12.addHandle)
      }
  
    // @LINE:135
    case controllers_WishlistController_list117_route(params@_) =>
      call { 
        controllers_WishlistController_list117_invoker.call(WishlistController_12.list)
      }
  
    // @LINE:136
    case controllers_WishlistController_details118_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_WishlistController_details118_invoker.call(WishlistController_12.details(id))
      }
  
    // @LINE:137
    case controllers_WishlistController_update119_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_WishlistController_update119_invoker.call(WishlistController_12.update(id))
      }
  
    // @LINE:138
    case controllers_WishlistController_updateHandle120_route(params@_) =>
      call { 
        controllers_WishlistController_updateHandle120_invoker.call(WishlistController_12.updateHandle)
      }
  
    // @LINE:139
    case controllers_WishlistController_delete121_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_WishlistController_delete121_invoker.call(WishlistController_12.delete(id))
      }
  
    // @LINE:141
    case controllers_RegisterController_submit122_route(params@_) =>
      call { 
        controllers_RegisterController_submit122_invoker.call(RegisterController_13.submit)
      }
  
    // @LINE:142
    case controllers_LoginController_submit123_route(params@_) =>
      call { 
        controllers_LoginController_submit123_invoker.call(LoginController_4.submit)
      }
  
    // @LINE:143
    case controllers_LoginController_signOut124_route(params@_) =>
      call { 
        controllers_LoginController_signOut124_invoker.call(LoginController_4.signOut)
      }
  
    // @LINE:144
    case controllers_SocialProviderController_authenticate125_route(params@_) =>
      call(params.fromPath[String]("provider", None)) { (provider) =>
        controllers_SocialProviderController_authenticate125_invoker.call(SocialProviderController_10.authenticate(provider))
      }
  }
}
