// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/nuxie/IdeaProjects/untitled12/conf/routes
// @DATE:Sat Sep 12 03:11:54 CEST 2020


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
